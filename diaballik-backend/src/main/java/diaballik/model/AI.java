package diaballik.model;

/**
 * this abstract class describes the strategy of an artificial intelligence (player)
 *
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public abstract class AI {

    public abstract Command play(final Board board, final int nbActions, final boolean isPlayerWhite);
    public AI() { }
}
