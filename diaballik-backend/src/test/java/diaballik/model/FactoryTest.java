package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

class FactoryTest {

    private Player p1;
    private Player p2;
    private Game game;
    private Command command;
    private Board board;

    @BeforeEach
    void setUp() {
    }

    @Test
    void createWhiteHumanPlayer() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");

        assertEquals(p1.getName(), "p1");
        assertTrue(p1.isWhite());
        assertTrue(p1.isHumanPlayer());
    }

    @Test
    void createBlackHumanPlayer() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "black");

        assertEquals(p1.getName(), "p1");
        assertFalse(p1.isWhite());
        assertTrue(p1.isHumanPlayer());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    void createAIPlayer(int level) {
        p1 = Factory.getINSTANCE().createPlayer(level, "p1", "white");

        assertEquals(p1.getName(), "p1");
        assertTrue(p1.isWhite());
        assertFalse(p1.isHumanPlayer());
    }

    @Test
    void createPlayerEmptyName() {
        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createPlayer(0, "", "white"));
    }

    @Test
    void createPlayerNullName() {
        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createPlayer(0, null, "white"));
    }

    @Test
    void createPlayerWrongColor() {
        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createPlayer(0, "p1", ""));
    }

    @Test
    void createPlayerNullColor() {
        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createPlayer(0, "p1", null));
    }

    @Test
    void createGame() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        game = Factory.getINSTANCE().createGame(p1, p2);

        assertEquals(game.getPlayers().size(), 2);
        assertEquals(game.getPlayers().get(0), p1);
        assertEquals(game.getPlayers().get(1), p2);
    }

    @Test
    void createGameNullPlayer1() {
        p1 = null;
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createGame(p1, p2));
    }

    @Test
    void createGameNullPlayer2() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = null;

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createGame(p1, p2));
    }

    @Test
    void createGameEqualPlayers() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createGame(p1, p2));
    }

    @Test
    void createCommandMoveBall() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        board = new Board(p1, p2);

        Optional<Piece> ballOwnerP1 = board.getPieces().stream()
                .filter(e -> e.getPlayer().equals(p1))
                .filter(e -> e.hasBall())
                .findFirst();

        command = Factory.getINSTANCE().createCommand(board, ballOwnerP1.get().getCoordinate(), new Coordinate(0, 0), p1.isWhite());
        assertTrue(command instanceof MoveBall);
    }

    @Test
    void createCommandMovePiece() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        board = new Board(p1, p2);

        Optional<Piece> notBallOwnerP1 = board.getPieces().stream()
                .filter(e -> e.getPlayer().equals(p1))
                .filter(e -> !e.hasBall())
                .findFirst();

        command = Factory.getINSTANCE().createCommand(board, notBallOwnerP1.get().getCoordinate(), new Coordinate(0, 0), p1.isWhite());
        assertTrue(command instanceof MovePiece);
    }

    @Test
    void createCommandNullBoard() {
        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createCommand(null, new Coordinate(0, 0), new Coordinate(0, 0), true));
    }

    @Test
    void createCommandNullCoordinate1() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        board = new Board(p1, p2);

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createCommand(board, null, new Coordinate(0, 0), true));
    }

    @Test
    void createCommandNullCoordinate2() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        board = new Board(p1, p2);

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createCommand(board, new Coordinate(0, 0), null, true));
    }

    @Test
    void createCommandWrongCoordinate1() {
        p1 = Factory.getINSTANCE().createPlayer(0, "p1", "white");
        p2 = Factory.getINSTANCE().createPlayer(0, "p2", "black");
        board = new Board(p1, p2);

        // we know there is no piece here
        Coordinate middle = new Coordinate(3, 3);

        assertThrows(IllegalArgumentException.class, () -> Factory.getINSTANCE().createCommand(board, middle, new Coordinate(0, 0), true));
    }
}