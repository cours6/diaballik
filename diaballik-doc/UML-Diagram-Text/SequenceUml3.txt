@startuml

actor frontend
activate frontend
activate ":GameRessources"
activate ":Factory"

frontend -> ":GameRessources": POST /game/new/{lv}/{n1}/{n2}/{c1}/{c2}

":GameRessources" -> ":Factory": createPlayer(0,n1,c1)
":Factory" -> "p1:Player": Player(c1,n1)
activate "p1:Player"
":Factory" --> ":GameRessources": p1:Player

":GameRessources" -> ":Factory": createPlayer(lv,n2,c2)

alt lv == 0

	":Factory" -> "p2:Player": Player(c2,n2)
	activate "p2:Player"

else
	":Factory" -> "p2:PlayerAI": PlayerAI(c2,n2,lv)
	activate "p2:PlayerAI"
	alt lv == 1
		"p2:PlayerAI" -> ":NoobAI": NoobAI()
		activate ":NoobAI"
	else lv == 2
		"p2:PlayerAI" -> ":StartingAI": StartingAI()
		activate ":StartingAI"
	else lv == 3
		"p2:PlayerAI" -> ":ProgressiveAI": ProgressiveAI()
		activate ":ProgressiveAI"
	end
	
end
":Factory" --> ":GameRessources": p2:Player

":GameRessources" -> ":Factory": createGame(p1,p2)
":Factory" -> ":Game":Game(p1,p2)
activate ":Game"

":Game" -> ":Turn":Turn(1)
activate ":Turn"

":Game" -> ":Board": Board(p1,p2)
activate ":Board"

loop x in range(0,6)
	":Board" -> ":Piece": Piece(p1,x,0)
	activate ":Piece"

	":Piece" -> ":Coordinate": Coordinate(x,0)
	activate ":Coordinate"
end

":Board" -> ":Piece": setBall(True)

loop x in range(0,6)
	":Board" -> ":Piece": Piece(p2,x,6)
	":Piece" -> ":Coordinate": Coordinate(x,6)

end

":Board" -> ":Piece": setBall(True)



":Factory" --> ":GameRessources": Game

":GameRessources" --> frontend: Game

deactivate ":GameRessources"
deactivate ":Factory"
deactivate "p1:Player"
deactivate "p2:Player"
deactivate ":Game"
deactivate frontend
deactivate "p2:PlayerAI"
deactivate ":NoobAI"
deactivate ":StartingAI"
deactivate ":ProgressiveAI"
deactivate ":Turn"
deactivate ":Board"
deactivate ":Piece"
deactivate ":Coordinate"
@enduml
