package diaballik.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Factory both uses factory and singleton pattern designs
 * INSTANCE is said to create complex objects
 * @author Quentin Mazouni
 * @version 1.0.1
 */
public final class Factory {

    private static final Factory INSTANCE = new Factory();
    private List<Integer> levels;

    private Factory() {
        this.levels = new ArrayList<Integer>(List.of(0, 1, 2, 3));
    }


    /**
     * @param level 0 => human player, 1/2/3 => ai player
     * @param name player's name
     * @param color player's color (has to be 'white' or 'black'
     * @return an instance of Player (Player or PlayerAI wrt level)
     * @throws IllegalArgumentException if name is null or empty string
     * if color is not 'white' or 'black'
     * if level doest not belong to [0, 3]
     * @since 1.0.0
     */
    public Player createPlayer(final int level, final String name, final String color) {
        if((!("white").equals(color) && !("black").equals(color))
                || (("").equals(name) || name == null)
                || (this.levels.stream().noneMatch(e -> e == level))) {
            throw new IllegalArgumentException();
        }

        if(level == 0) {
            return new Player(("white").equals(color), name);
        } else {
            return new PlayerAI(("white").equals(color), name, level);
        }
    }

    /**
     * @param p1  player1
     * @param p2 player2
     * @return an instance of Game
     * @throws IllegalArgumentException if one of the players are null
     * if p1 equals p2
     * @since 1.0.0
     */
    public Game createGame(final Player p1, final Player p2) {
        if(p1 == null || p2 == null || p1.equals(p2)) {
            throw new IllegalArgumentException();
        }

        return new Game(p1, p2);
    }


    /**
     * @param board current game's board
     * @param c1 current coordinate
     * @param c2 new coordinate
     * @param isWhite player's color
     * @return an instance of an undoable/redoable Command
     * @throws IllegalArgumentException if one of the parameters is null
     * if no piece has its coordinate equal to c1
     * @since 1.0.0
     */
    public Command createCommand(final Board board, final Coordinate c1, final Coordinate c2, final Boolean isWhite) {
        if(board == null || c1 == null || c2 == null) {
            throw new IllegalArgumentException();
        }

        final Optional<Piece> p = board.getPieces().stream().filter(e -> e.getCoordinate().equals(c1)).findFirst();

        if(p.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            return p.get().hasBall() ? new MoveBall(c1, c2, isWhite) : new MovePiece(c1, c2, isWhite);
        }
    }

    /**
     * getter of the instance
     * @return the Factory
     * @since 1.0.1
     */
    public static Factory getINSTANCE() {
        return Factory.INSTANCE;
    }
}
