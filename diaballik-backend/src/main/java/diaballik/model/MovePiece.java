package diaballik.model;

import java.util.Optional;

/**
 * this Class is a command which represents a piece Movement
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public class MovePiece extends Command {

    /**
     * Constructor For MovePiece
     * @param oldC The coordinate of the piece which has to move
     * @param newC The new coordinate of the piece
     * @param isWhite color of the player of the turn
     * @since 1.0.0
     */
    public MovePiece(final Coordinate oldC, final Coordinate newC, final boolean isWhite) {
        super(oldC, newC, isWhite);
    }

    /**
     * does the action
     * @param board the current board of the game
     */
    @Override
    public void doS(final Board board) {
        if(this.canDo(board)) {
            board.getPieces()
                    .stream()
                    .filter(p -> p.getCoordinate().equals(super.getOldCoordinate()))
                    .forEach(p -> p.setCoordinate(super.getNewCoordinate()));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * the function is used to know if the movement is possible
     * @param board board to modify
     * @return true if the movement is possible
     * @since 1.0.0
     */
    @Override
    public boolean canDo(final Board board) {
        if(board == null) {
            throw new IllegalArgumentException();
        }

        final Optional<Piece> oldPiece = board.getPieces()
                .stream()
                .filter(c -> c.getCoordinate().equals(super.getOldCoordinate()))
                .findFirst();

        final boolean color = oldPiece.isPresent()
                && oldPiece.get().isWhite() == super.isPlayerIsWhite();

        final boolean hasBall = oldPiece.isPresent() && oldPiece.get().hasBall();

        final boolean oldCOccupied = oldPiece.isPresent();

        final long numberPieceOnNewC = board.getPieces()
                .stream()
                .filter(c -> c.getCoordinate().equals(super.getNewCoordinate()))
                .count();
        final boolean NewCFree = numberPieceOnNewC == 0;

        final boolean areAdjacent = this.areAdjacent();

        return color && !hasBall && oldCOccupied && NewCFree && areAdjacent;
    }

    /**
     * @return true if the old and new coordinates are adjacent
     */
    private boolean areAdjacent() {
        final boolean onSameAdjacentRow = getNewCoordinate().onSameRow(getOldCoordinate())
                && Math.abs(getNewCoordinate().getX() - getOldCoordinate().getX()) == 1;

        final boolean onSameAdjacentColumn = getNewCoordinate().onSameColumn(getOldCoordinate())
                && Math.abs(getNewCoordinate().getY() - getOldCoordinate().getY()) == 1;

        return (onSameAdjacentRow || onSameAdjacentColumn);
    }

    /**
     * undoes the action
     * @param board
     * @since 1.0.0
     */
    @Override
    public void undo(final Board board) {
        final MovePiece tmp = new MovePiece(super.getNewCoordinate(), super.getOldCoordinate(), super.isPlayerIsWhite());
        tmp.doS(board);
    }
    
    /**
     * redoes the action
     * @param board
     * @since 1.0.0
     */
    @Override
    public void redo(final Board board) {
        this.doS(board);
    }
}
