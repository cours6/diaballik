package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovePieceTest {

    Board b;
    Player p1;
    Player p2;


    @BeforeEach
    void setUp(){
        p1 = new Player(true, "Toto");
        p2 = new Player(false, "Tata");
        b = new Board(p1, p2);
    }

    @Test
    void doS() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);
        c.doS(b);
        assertTrue(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(newC)));
        assertFalse(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(oldC)));
    }

    @Test
    void doSException() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,2);
        Command c = new MovePiece(oldC, newC, false);
        assertThrows(IllegalArgumentException.class, () -> c.doS(b));
    }

    @Test
    void canDoTrueColumn() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);
        assertTrue(c.canDo(b));
    }

    @Test
    void canDoTrueRow() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);
        c.doS(b);
        Coordinate newC2 = new Coordinate(1,1);
        Command c2 = new MovePiece(newC, newC2,false);
        assertTrue(c2.canDo(b));
    }

    @Test
    void canDoFalseBadPlayer() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, true);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseBadOldCoordinate() {
        Coordinate oldC = new Coordinate(0,1);
        Coordinate newC = new Coordinate(0,2);
        Command c = new MovePiece(oldC, newC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseBadNewCoordinateColumn() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,2);
        Command c = new MovePiece(oldC, newC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseBadNewCoordinateRow() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);
        c.doS(b);
        Coordinate newC2 = new Coordinate(2,1);
        Command c2 = new MovePiece(newC, newC2, false);
        assertFalse(c2.canDo(b));
    }

    @Test
    void canDoFalseBadNewCoordinateDiagonal() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(1,1);
        Command c = new MovePiece(oldC, newC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseOccupiedNewCoordinate() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(1,0);
        Command c = new MovePiece(oldC, newC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseSameCoordinate() {
        Coordinate oldC = new Coordinate(0,0);
        Command c = new MovePiece(oldC, oldC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void canDoFalseHasBall() {
        Coordinate oldC = new Coordinate(3,0);
        Coordinate newC = new Coordinate(3,1);
        Command c = new MovePiece(oldC, newC, false);
        assertFalse(c.canDo(b));
    }

    @Test
    void constructorExceptionOldNull() {
        Coordinate newC = new Coordinate(3,1);
        assertThrows(IllegalArgumentException.class, () -> new MovePiece(null, newC, false));
    }

    @Test
    void constructorExceptionNewNull() {
        Coordinate oldC = new Coordinate(3,1);
        assertThrows(IllegalArgumentException.class, () -> new MovePiece(oldC, null, false));
    }

    @Test
    void undo() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);

        c.doS(b);
        c.undo(b);

        assertTrue(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(oldC)));
        assertFalse(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(newC)));
    }

    @Test
    void redo() {
        Coordinate oldC = new Coordinate(0,0);
        Coordinate newC = new Coordinate(0,1);
        Command c = new MovePiece(oldC, newC, false);

        c.doS(b);
        c.undo(b);
        c.redo(b);

        assertTrue(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(newC)));
        assertFalse(b.getPieces().stream().anyMatch(e -> e.getCoordinate().equals(oldC)));
    }
}