import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { StartGameComponent } from './components/start-game/start-game.component';
import { DataProviderService } from './services/data-provider.service';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { PlayGameComponent } from './components/play-game/play-game.component';
import { EndGameComponent } from './components/end-game/end-game.component';


@NgModule({
  declarations: [
    AppComponent,
    StartGameComponent,
    NotFoundPageComponent,
    PlayGameComponent,
    EndGameComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DataProviderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
