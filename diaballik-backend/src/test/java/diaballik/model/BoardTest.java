package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    Board b;
    Player p1;
    Player p2;

    @BeforeEach
    void setUp() {
        p1 = new Player(true, "toto");
        p2 = new Player(false, "tata");
        b = new Board(p1, p2);
    }

    @Test
    void sizePiece() {
        assertEquals(14, b.getPieces().size());
    }

    @Test
    void nbP1Piece() {
        long nb = b.getPieces()
                .stream()
                .filter((p) -> p.getPlayer().equals(p1))
                .count();
        assertEquals(7, nb);
    }

    @Test
    void nbP2Piece() {
        long nb = b.getPieces()
                .stream()
                .filter((p) -> p.getPlayer().equals(p2))
                .count();
        assertEquals(7, nb);
    }

    @Test
    void nbP1Ball(){
        long nb = b.getPieces()
                .stream()
                .filter((p) -> p.getPlayer().equals(p1))
                .filter((p) -> p.hasBall())
                .count();
        assertEquals(1, nb);
    }

    @Test
    void nbP2Ball(){
        long nb = b.getPieces()
                .stream()
                .filter((p) -> p.getPlayer().equals(p2))
                .filter((p) -> p.hasBall())
                .count();
        assertEquals(1, nb);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6})
    void CoordinatePieceP1Test(int x){

        long nb = b.getPieces()
                .stream()
                .filter( p -> p.getPlayer().equals(p1))
                .filter( p -> p.getCoordinate().getX() == x)
                .filter( p -> p.getCoordinate().getY() == 6)
                .count();

        assertEquals(1, nb);

    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6})
    void CoordinatePieceP2Test(int x){

        long nb = b.getPieces()
                .stream()
                .filter( p -> p.getPlayer().equals(p2))
                .filter( p -> p.getCoordinate().getX() == x)
                .filter( p -> p.getCoordinate().getY() == 0)
                .count();

        assertEquals(1, nb);

    }

    @Test
    void isVictoryAchievedFalse() {
        assertFalse(b.isVictoryAchieved());
    }

    @Test
    void isVictoryAchievedTrueP1() {
        b.getPieces().get(3).setCoordinate(new Coordinate(0,0));
        assertTrue(b.isVictoryAchieved());
    }

    @Test
    void isVictoryAchievedTrueP2() {
        b.getPieces().get(10).setCoordinate(new Coordinate(0,6));
        assertTrue(b.isVictoryAchieved());
    }
}