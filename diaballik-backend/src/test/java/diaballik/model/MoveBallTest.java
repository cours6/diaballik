package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoveBallTest {

    private Board board;
    private Player p1;
    private Player p2;
    private Coordinate ballC;

    @BeforeEach
    void setUp(){
        this.p1 = new Player(true, "Toto");
        this.p2 = new Player(false, "Tata");
        this.board = new Board(p1, p2);
        // the piece at this coordinate has the ball
        this.ballC = new Coordinate(3, 0);
    }

    @Test
    void MoveBallNullOldCoordinate() {
        assertThrows(IllegalArgumentException.class, () -> new MoveBall(null, this.ballC, this.p2.isWhite()));
    }

    @Test
    void MoveBallNullNewCoordinate() {
        assertThrows(IllegalArgumentException.class, () -> new MoveBall(this.ballC, null, this.p2.isWhite()));
    }

    @Test
    void doSRow() {
        Coordinate c1 = new Coordinate(2,0);
        Command command = new MoveBall(this.ballC, c1, this.p2.isWhite());

        command.doS(this.board);

        assertFalse(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(this.ballC)).findFirst().get().hasBall());
        assertTrue(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(c1)).findFirst().get().hasBall());
    }

    @Test
    void doSColumn() {
        Coordinate c1 = new Coordinate(2,0);
        Coordinate c2 = new Coordinate(2,1);
        Coordinate c3 = new Coordinate(2,2);
        Coordinate c4 = new Coordinate(3,2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());
        Command command3 = new MovePiece(c3, c4, this.p2.isWhite());
        Command command4 = new MoveBall(this.ballC, c4, this.p2.isWhite());

        command1.doS(this.board);
        command2.doS(this.board);
        command3.doS(this.board);
        command4.doS(this.board);

        assertFalse(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(this.ballC)).findFirst().get().hasBall());
        assertTrue(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(c4)).findFirst().get().hasBall());
    }

    @Test
    void doSDiag() {
        Coordinate c1 = new Coordinate(1,0);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(1,2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());
        Command command3 = new MoveBall(this.ballC, c3, this.p2.isWhite());

        command1.doS(this.board);
        command2.doS(this.board);
        command3.doS(this.board);

        assertFalse(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(this.ballC)).findFirst().get().hasBall());
        assertTrue(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(c3)).findFirst().get().hasBall());
    }

    @Test
    void doSException() {
        Coordinate c1 = new Coordinate(2,0);
        Command command = new MoveBall(this.ballC, c1, this.p2.isWhite());

        assertThrows(IllegalArgumentException.class, () -> command.doS(null));
    }

    // we do not check cases where doS cannot be done (because we test them in canDo)

    @Test
    void canDoNull() {
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(1,0);
        Command command = new MoveBall(c1, c2, this.p2.isWhite());

        assertThrows(IllegalArgumentException.class, () -> command.canDo(null));
    }

    @Test
    void canDoNoBallOldCoordinate() {
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(1,0);
        Command command = new MoveBall(c1, c2, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoWrongOldCoordinate() {
        Coordinate c1 = new Coordinate(2,1);
        Coordinate c2 = new Coordinate(2,0);
        Command command = new MoveBall(c1, c2, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoWrongPlayer() {
        Coordinate c1 = new Coordinate(3,6);
        Coordinate c2 = new Coordinate(2,6);
        Command command = new MoveBall(c1, c2, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoWrongNewCoordinate() {
        Coordinate c1 = new Coordinate(2,1);
        Command command = new MoveBall(this.ballC, c1, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoWrongSamePiece() {
        Command command = new MoveBall(this.ballC, this.ballC, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoNoFreeRow() {
        Coordinate c1 = new Coordinate(0,0);
        Command command = new MoveBall(this.ballC, c1, this.p2.isWhite());

        assertFalse(command.canDo(this.board));
    }

    @Test
    void canDoNoFreeColumn() {
        Coordinate c1 = new Coordinate(2,0);
        Coordinate c2 = new Coordinate(2,1);
        Coordinate c3 = new Coordinate(3,1);
        Coordinate c4 = new Coordinate(4,0);
        Coordinate c5 = new Coordinate(4,1);
        Coordinate c6 = new Coordinate(4,2);
        Coordinate c7 = new Coordinate(3,2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());
        Command command3 = new MovePiece(c4, c5, this.p2.isWhite());
        Command command4 = new MovePiece(c5, c6, this.p2.isWhite());
        Command command5 = new MovePiece(c6, c7, this.p2.isWhite());
        Command command6 = new MoveBall(this.ballC, c7, this.p2.isWhite());

        command1.doS(this.board);
        command2.doS(this.board);
        command3.doS(this.board);
        command4.doS(this.board);
        command5.doS(this.board);

        assertFalse(command6.canDo(this.board));
    }

    @Test
    void canDoNoFreeDiag() {
        Coordinate c1 = new Coordinate(2,0);
        Coordinate c2 = new Coordinate(2,1);
        Coordinate c3 = new Coordinate(1,0);
        Coordinate c4 = new Coordinate(1,1);
        Coordinate c5 = new Coordinate(1,2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c3, c4, this.p2.isWhite());
        Command command3 = new MovePiece(c4, c5, this.p2.isWhite());
        Command command4 = new MoveBall(this.ballC, c5, this.p2.isWhite());

        command1.doS(this.board);
        command2.doS(this.board);
        command3.doS(this.board);

        assertFalse(command4.canDo(this.board));
    }

    @Test
    void undo() {
        Coordinate c1 = new Coordinate(4,0);

        Command command1 = new MoveBall(this.ballC, c1, this.p2.isWhite());

        command1.doS(this.board);
        command1.undo(this.board);

        assertTrue(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(this.ballC)).findFirst().get().hasBall());
        assertFalse(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(c1)).findFirst().get().hasBall());
    }

    @Test
    void undoException() {
        Coordinate c1 = new Coordinate(4,0);

        Command command1 = new MoveBall(this.ballC, c1, this.p2.isWhite());

        assertThrows(IllegalArgumentException.class, () -> command1.undo(null));
    }

    @Test
    void redo() {
        Coordinate c1 = new Coordinate(4,0);

        Command command1 = new MoveBall(this.ballC, c1, this.p2.isWhite());

        command1.doS(this.board);
        command1.undo(this.board);
        command1.redo(this.board);

        assertFalse(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(this.ballC)).findFirst().get().hasBall());
        assertTrue(this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(c1)).findFirst().get().hasBall());
    }

    @Test
    void redoException() {
        Coordinate c1 = new Coordinate(4,0);

        Command command1 = new MoveBall(this.ballC, c1, this.p2.isWhite());

        assertThrows(IllegalArgumentException.class, () -> command1.undo(null));
    }

    @Test
    void moveBallBadPlace(){
        Coordinate c2 = new Coordinate(2,1);
        Coordinate c1 = new Coordinate(2,0);
        Command command = new MovePiece(c1, c2, this.p2.isWhite());
        command.doS(this.board);

        Coordinate c3 = new Coordinate(2, 2);
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());
        command2.doS(this.board);

        Command command3 = new MoveBall(this.ballC, c3, this.p2.isWhite());

        assertFalse(command3.canDo(this.board));
    }
}