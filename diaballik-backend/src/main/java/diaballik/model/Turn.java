package diaballik.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * this class represents a turn
 * @author Quentin Mazouni
 * @version 1.1.0
 */
public class Turn {

    private int nbActions;
    private int turnNumber;
    private List<Command> commands;
    private Player player;

    private static final int MAX_ACTIONS = 3;

    /**
     * @param turnNumber the number of the turn
     * @throws IllegalArgumentException if turnNumber is negative or zero
     * @since 1.0.0
     */
    public Turn(final int turnNumber) {
        if(turnNumber <= 0) {
            throw new IllegalArgumentException();
        }

        this.turnNumber = turnNumber;
        this.nbActions = 0;
        this.commands = new ArrayList<>();
        IntStream.range(0, Turn.MAX_ACTIONS)
                .forEach(i -> this.commands.add(null));
        this.player = null;
    }


    /**
     * describes the state of the turn
     * @return true is the current is finished (all the actions have been done)
     * @since 1.0.0
     */
    public boolean isFinish() {
        return (this.nbActions == 3);
    }

    /**
     * undoes the last current player's move
     * if no move has been done yet, does nothing
     * @param board the current board
     * @since 1.0.0
     * @throws IllegalArgumentException if the parameter is null
     * @throws IllegalAccessException if there is no move to undo
     */
    public void undo(final Board board) throws IllegalAccessException {
        if(board == null) {
            throw new IllegalArgumentException();
        }

        if(this.nbActions == 0) {
            throw new IllegalAccessException();
        }

        this.commands.get(this.nbActions - 1).undo(board);
        this.nbActions--;
    }

    /**
     * redoes the last current player's move
     * @param board the current board
     * @since 1.0.0
     * @throws IllegalArgumentException if the parameter is null
     * @throws IllegalAccessException if there is no move to redo
     */
    public void redo(final Board board) throws IllegalAccessException {
        if(board == null) {
            throw new IllegalArgumentException();
        }

        if(this.isFinish() || this.commands.get(this.nbActions) == null) {
            throw new IllegalAccessException();
        }

        this.commands.get(this.nbActions).redo(board);
        this.nbActions++;
    }

    /**
     * updates the board wrt the command
     * @param board the current board to modify
     * @param command the current player's move to apply to the board
     * @throws IllegalArgumentException if one the parameters is null
     * @throws IllegalAccessException if the player can no longer play another move
     * @since 1.0.0
     */
    public void play(final Board board, final Command command) throws IllegalAccessException {
        if(board == null || command == null) {
            throw new IllegalArgumentException();
        }

        if(this.isFinish()) {
            throw new IllegalAccessException();
        }

        // do not catch exceptions yet to let the RESTRessources manage them by try/catch
        command.doS(board);
        this.nbActions++;
        this.commands.add(this.nbActions - 1, command);

        // once a move is played, the player can now only undo
        IntStream.range(this.nbActions, Turn.MAX_ACTIONS)
                .forEach(i -> this.commands.add(i, null));
    }

    /**
     * @return the number of actions that have been played during this turn
     * @since 1.0.0
     */
    public int getNbActions() {
        return this.nbActions;
    }

    /**
     * @return the player who is currently playing
     * @since 1.0.0
     */
    public Player getPlayer() {
        return this.player;
    }

    /**
     * @param player current player
     * @throws IllegalArgumentException if player is null
     * @since 1.1.0
     */
    public void setPlayer(final Player player) {
        if(player == null) {
            throw new IllegalArgumentException();
        }

        this.player = player;
    }
}
