package diaballik.model;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestMarshalling {
	<T> T marshall(final T objectToMarshall) throws IOException, JAXBException {
		final Map<String, Object> properties = new HashMap<>();
		properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
		properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);

		final JAXBContext ctx = JAXBContextFactory.createContext(new Class[]{objectToMarshall.getClass()}, properties);
		final Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		final StringWriter sw = new StringWriter();
		marshaller.marshal(objectToMarshall, sw);
		marshaller.marshal(objectToMarshall, System.out);

		final Unmarshaller unmarshaller = ctx.createUnmarshaller();
		final StringReader reader = new StringReader(sw.toString());
		final T o = (T) unmarshaller.unmarshal(reader);

		sw.close();
		reader.close();

		return o;
	}

	Player p1;
	Player p2;
	Player p3;
	Coordinate c1;
	Piece pi1;
	Piece pi2;
	Board b1;
	Game g1;

	@BeforeEach
	void setUp() {
		p1 = new Player(false, "Toto");
		p2 = new Player(true, "Titi");
		p3 = new PlayerAI(true , "Tata", 1);
		c1 = new Coordinate(3, 5);
		pi1 = new Piece(p2, c1.getX(), c1.getY());
		pi2 = new Piece(p2, c1.getX(), c1.getY());
		pi2.setBall(true);
		b1 = new Board(p1, p2);
		g1 = new Game(p1, p2);
	}

	@Test
	void testPlayer1() throws IOException, JAXBException {
		final Player p = marshall(p1);
		assertEquals("Toto", p.getName());
		assertEquals(false, p.isWhite());
		assertEquals(false, p.getWin());
	}

	@Test
	void testPlayer2() throws IOException, JAXBException {
		final Player p = marshall(p2);
		assertEquals("Titi", p.getName());
		assertEquals(true, p.isWhite());
		assertEquals(false, p.getWin());
	}

	@Test
	void testPlayer3() throws IOException, JAXBException {
		final Player p = marshall(p3);
		assertEquals("Tata", p.getName());
		assertEquals(true, p.isWhite());
		assertEquals(false, p.getWin());
	}

	@Test
	void testCoordinate() throws IOException, JAXBException {
		final Coordinate c = marshall(c1);
		assertEquals(c1.getY(), c.getY());
		assertEquals(c1.getX(), c.getX());
	}

	@Test
	void testPiece1() throws IOException, JAXBException {
		final Piece p = marshall(pi1);
		assertEquals(c1, p.getCoordinate());
		assertEquals(p2, p.getPlayer());
		assertEquals(false, p.hasBall());
	}

	@Test
	void testPiece2() throws IOException, JAXBException {
		final Piece p = marshall(pi2);
		assertEquals(true, p.hasBall());
	}

	@Test
	void testBoard() throws IOException, JAXBException, NoSuchFieldException, IllegalAccessException {
		final Board b = marshall(b1);

		Field fieldP1 = b.getClass().getDeclaredField("p1");
		Field fieldP2 = b.getClass().getDeclaredField("p2");
		fieldP1.setAccessible(true);
		fieldP2.setAccessible(true);

		assertEquals(p1, fieldP1.get(b));
		assertEquals(p2, fieldP2.get(b));

		assertEquals(14, b.getPieces().size());
	}

	@Test
	void testGame() throws IOException, JAXBException {
		g1.newTurn();
		final Game g = marshall(g1);
		assertEquals(p1, g.getPlayers().get(0));
		assertEquals(p2, g.getPlayers().get(1));
		assertEquals(14, g.getBoard().getPieces().size());
	}
}
