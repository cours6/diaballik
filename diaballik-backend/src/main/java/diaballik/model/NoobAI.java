package diaballik.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * this represents the noob artificial intelligence
 * which plays randomly (ie makes randomly selected actions)
 * @author Quentin Mazouni
 * @version 1.0.0
 */
public class NoobAI extends AI {

    public NoobAI() {
        super();
    }

    /**
     * makes the ai play a move
     * here the action is randomly chosen
     * @param board current board of the game
     * @param nbActions number of actions left to be played during the current turn
     * @param isPlayerWhite player's color for whom the AI plays
     * @return a command which describes the ai's move
     * @throws IllegalArgumentException if the board is null
     * @since 1.0.0
     */
    @Override
    public Command play(final Board board, final int nbActions, final boolean isPlayerWhite) {
        if(board == null) {
            throw new IllegalArgumentException();
        }

        final Command command;
        final List<Piece> pieces = board.getPieces().stream().filter(e -> e.isWhite() == isPlayerWhite).collect(Collectors.toList());
        final Piece piece = pieces.get((int) (Math.random() * pieces.size()));

        if(piece.hasBall()) {
            // if the randomly chosen piece has the ball, then we try to give it to one of the remaining pieces...
            pieces.remove(piece);
            command = new MoveBall(piece.getCoordinate(), pieces.get((int) (Math.random() * pieces.size())).getCoordinate(), isPlayerWhite);
        } else {
            // else, we try to randomly move the chosen piece
            final int x = piece.getCoordinate().getX();
            final int y = piece.getCoordinate().getY();
            final List<Coordinate> directions = new ArrayList<>();

            if(x != 0) {
                directions.add(new Coordinate(x - 1, y));
            }
            if(x != 6) {
                directions.add(new Coordinate(x + 1, y));
            }
            if(y != 0) {
                directions.add(new Coordinate(x, y - 1));
            }
            if(y != 6) {
                directions.add(new Coordinate(x, y + 1));
            }

            command = new MovePiece(piece.getCoordinate(), directions.get((int) (Math.random() * directions.size())), isPlayerWhite);
        }

        return command.canDo(board) ? command : this.play(board, nbActions, isPlayerWhite);
    }
}
