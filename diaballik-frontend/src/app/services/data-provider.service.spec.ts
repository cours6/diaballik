import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DataProviderService } from './data-provider.service';
import { IGame } from '../interfaces/igame';

describe('DataProviderService', () => {
    let service: DataProviderService;
    let httpTestingController: HttpTestingController;
    let game: IGame;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                DataProviderService
            ]
        });

        service = TestBed.get(DataProviderService);
        httpTestingController = TestBed.get(HttpTestingController);

        game = {
            'nbTurn': 1,
            'players': [
                {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                }
            ],
            'board': {
                'pieces': [
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 0,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 1,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 2,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': true,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 3,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 4,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 5,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': true,
                        'player': {
                            'name': 'a',
                            'hasWon': false,
                            'isWhite': true
                        },
                        'coordinate': {
                            'x': 6,
                            'y': 6
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 0,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 1,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 2,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': true,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 3,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 4,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 5,
                            'y': 0
                        }
                    },
                    {
                        'hasBall': false,
                        'isWhite': false,
                        'player': {
                            'name': 'b',
                            'hasWon': false,
                            'isWhite': false
                        },
                        'coordinate': {
                            'x': 6,
                            'y': 0
                        }
                    }
                ],
                'p1': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'p2': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                }
            },
            'indexCurrentPlayer': 0,
            'nbActions': 0
        };
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should create', () => {
        expect(service).toBeTruthy();
        expect(httpTestingController).toBeTruthy();
    });

    it('should put reset', () => {
        const httpResponse = game;

        service.putReset().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'PUT', url: '/game/reset' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should put redo', () => {
        const httpResponse = game;

        service.putRedo().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'PUT', url: '/game/redo' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should put undo', () => {
        const httpResponse = game;

        service.putUndo().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'PUT', url: '/game/undo' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should put endTurn', () => {
        const httpResponse = game;

        service.putEndTurn().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'PUT', url: '/game/endTurn' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should post newPlay', () => {
        const httpResponse = game;

        service.putPlay(0, 6, 0, 5).subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'PUT', url: '/game/play/0/6/0/5' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should post newGame', () => {
        const httpResponse = game;

        service.postNewGame(0, 'a', 'white', 'b', 'black').subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'POST', url: '/game/new/0/a/white/b/black' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should get victory', () => {
        const httpResponse = 'false';

        service.getVictory().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'GET', url: '/game/victory' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should get nbActions', () => {
        const httpResponse = '4';

        service.getNbActions().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'GET', url: '/game/nbActions' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should get game', () => {
        const httpResponse = game;

        service.getGame().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'GET', url: '/game/current' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should get isAIPlayer', () => {
        const httpResponse = 'false';

        service.getIsAIPlayer().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'GET', url: '/game/isAIPlayer' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });

    it('should get aiPlayerMove', () => {
        const httpResponse = game;

        service.getAIPlayerMove().subscribe(result => {
            expect(result).toBeDefined();
            expect(result).toEqual(httpResponse);
        });

        const req = httpTestingController.expectOne({ method: 'GET', url: '/game/aiPlayerMove' });
        const body = req.request.body;
        expect(body).toBeDefined();
        req.flush(httpResponse);
    });
});