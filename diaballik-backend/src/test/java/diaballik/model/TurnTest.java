package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TurnTest {

    private Turn turn;
    private Player p1;
    private Player p2;
    private Board board;

    @BeforeEach
    void setUp() {
        this.turn = new Turn(1);
        this.p1 = new Player(true, "player1");
        this.p2 = new Player(false, "player2");
        this.board = new Board(this.p1, this.p2);
    }

    @Test
    void isFinishFalse() {
        assertFalse(this.turn.isFinish());
    }

    @Test
    void isFinishTrue() throws IllegalAccessException {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Coordinate c3 = new Coordinate(0, 2);
        Coordinate c4 = new Coordinate(0, 3);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());
        Command command3 = new MovePiece(c3, c4, this.p2.isWhite());

        assertFalse(this.turn.isFinish());
        this.turn.play(this.board, command1);
        assertFalse(this.turn.isFinish());
        this.turn.play(this.board, command2);
        assertFalse(this.turn.isFinish());
        this.turn.play(this.board, command3);
        assertTrue(this.turn.isFinish());
    }

    @Test
    void undo() throws IllegalAccessException {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());

        this.turn.play(this.board, command1);
        this.turn.undo(this.board);

        assertTrue(this.board.getPieces().stream().noneMatch(e -> e.getCoordinate().equals(c2)));
        assertEquals(0, this.turn.getNbActions());
    }

    @Test
    void undoTwice() throws IllegalAccessException {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Coordinate c3 = new Coordinate(0, 2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());

        this.turn.play(this.board, command1);
        this.turn.play(this.board, command2);

        this.turn.undo(this.board);
        this.turn.undo(this.board);

        assertTrue(this.board.getPieces().stream().noneMatch(e -> e.getCoordinate().equals(c2)));
        assertEquals(0, this.turn.getNbActions());
    }

    @Test
    void undoNoMove() {
        assertThrows(IllegalAccessException.class, () -> this.turn.undo(board));
    }

    @Test
    void redo() throws IllegalAccessException {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());

        this.turn.play(this.board, command1);
        this.turn.undo(this.board);
        this.turn.redo(this.board);

        assertTrue(this.board.getPieces().stream().noneMatch(e -> e.getCoordinate().equals(c1)));
        assertEquals(1, this.turn.getNbActions());
    }

    @Test
    void redoExtremeCase() {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Coordinate c3 = new Coordinate(0, 2);

        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());
        Command command2 = new MovePiece(c2, c3, this.p2.isWhite());

        try {
            this.turn.play(this.board, command1);
            this.turn.play(this.board, command2);

            this.turn.undo(this.board);
            this.turn.undo(this.board);

            this.turn.play(this.board,command1);
        } catch (IllegalAccessException e) {
            fail();
        }

        assertThrows(IllegalAccessException.class, () -> this.turn.redo(board));
    }

    @Test
    void playCommandNull() {
        assertThrows(IllegalArgumentException.class, () -> this.turn.play(this.board, null));
    }

    @Test
    void playBoardNull() {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);
        Command command1 = new MovePiece(c1, c2, this.p2.isWhite());

        assertThrows(IllegalArgumentException.class, () -> this.turn.play(null, command1));
    }

    @Test
    void play() throws IllegalAccessException {
        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(0, 1);

        Command command = new MovePiece(c1, c2, this.p2.isWhite());

        this.turn.play(this.board, command);
        assertEquals(this.turn.getNbActions(), 1);
        assertTrue(this.board.getPieces().stream().noneMatch(e -> e.getCoordinate().equals(c1)));
    }

    @Test
    void getNbActions() {
        assertEquals(this.turn.getNbActions(), 0);
    }

    @Test
    void getPlayerNull() {
        assertNull(this.turn.getPlayer());
    }

    @Test
    void getPlayer() {
        this.turn.setPlayer(this.p1);

        assertEquals(this.turn.getPlayer(), this.p1);
    }

    @Test
    void setPlayerNull() {
        assertThrows(IllegalArgumentException.class, () -> this.turn.setPlayer(null));
    }

    @Test
    void setPlayer() {
        this.turn.setPlayer(this.p1);

        assertEquals(this.turn.getPlayer(), this.p1);
    }
}