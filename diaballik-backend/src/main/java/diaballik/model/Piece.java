package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * this class represents a Piece on the Board
 * @author Guénaël Bournac
 * @version 1.1.1
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Piece {

    /**
     * This attribute is true if the piece contains the ball
     * @since 1.0.0
     */
    private boolean hasBall;

    /**
     * This attribute is true if the owner is white
     * @since 1.0.0
     */
    private boolean isWhite;

    /**
     * This attribute contains the owner of the piece
     * @since 1.0.0
     */
    private Player player;


    /**
     * This attribute is the coordinate of the piece
     * @since 1.0.0
     */
    private Coordinate coordinate;

    /**
     * Getter for isWhite
     * @return isWhite
     * @since 1.0.0
     */
    public boolean isWhite() {
        return this.isWhite;
    }

    /**
     * Setter for coordinate
     * @param coordinate the coordinate to set
     * @since 1.0.0
     */
    public void setCoordinate(final Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * Getter for coordinate
     * @return the coordinate
     * @since 1.0.0
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * Compares two Piece to if they are on the same Row
     * @param p piece to compare
     * @return true if pieces are on the same row
     * @since 1.1.1
     */
    public boolean onSameRow(final Piece p) {
        if (p == null) {
            return false;
        }
        return this.coordinate.onSameRow(p.coordinate);
    }

    /**
     * Compares two Piece to if they are on the same column
     * @param p piece to compare
     * @return true if pieces are on the same column
     * @since 1.1.1
     */
    public boolean onSameColumn(final Piece p) {
        if (p == null) {
            return false;
        }
        return this.coordinate.onSameColumn(p.coordinate);
    }

    /**
     * Compares two Piece to if they are on the same diagonal
     * @param p piece to compare
     * @return true if pieces are on the same diagonal
     * @since 1.1.1
     */
    public boolean onSameDiagonal(final Piece p) {
        if (p == null) {
            return false;
        }
        return this.coordinate.onSameDiagonal(p.coordinate);
    }

    /**
     * Getter for player
     * @return the player
     * @since 1.0.0
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * setter for hasBall
     * @since 1.0.0
     */
    public void setBall(final boolean hasBall) {
        this.hasBall = hasBall;
    }

    /**
     * Getter for hasBall
     * @return the player
     * @since 1.0.0
     */
    public boolean hasBall() {
        return hasBall;
    }

    /**
     * Constructor for Piece
     * @param player the owner of the piece
     * @since 1.0.0
     */
    public Piece(final Player player) {
        this.hasBall = false;
        this.player = player;
        this.coordinate = new Coordinate(0, 0);
        this.isWhite = player.isWhite();
    }

    /**
     * Constructor for Piece
     * @param player the owner of the piece
     * @param x x Coordinate of the piece
     * @param y y Coordinate of the piece
     * @since 1.1.0
     */
    public Piece(final Player player, final int x, final int y) {
        this.hasBall = false;
        this.player = player;
        this.coordinate = new Coordinate(x, y);
        this.isWhite = player.isWhite();
    }

    private Piece() {
        this.hasBall = false;
        this.player = null;
        this.coordinate = null;
        this.isWhite = false;
    }
}
