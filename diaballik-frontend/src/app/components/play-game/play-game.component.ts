import { Component, OnInit } from '@angular/core';
import { IGame, IPiece } from 'src/app/interfaces/igame';
import { DataProviderService } from 'src/app/services/data-provider.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-play-game',
  templateUrl: './play-game.component.html',
  styleUrls: ['./play-game.component.css']
})
export class PlayGameComponent implements OnInit {

  game: IGame = undefined;
  board: Array<IPiece> = [];
  isGameFinished: boolean = false;
  isAIPlayer: boolean = false;
  current:
    {
      piece: IPiece,
      x: number,
      y: number
    };

  constructor(private dataProviderService: DataProviderService, private router: Router) { }

  ngOnInit() {
    this.dataProviderService.getGame()
    .subscribe(result => {
      this.game = result;
      this.initBoard();
      this.current = {
        piece: undefined,
        x: 0, y: 0
      };
    }, error => {
      this.game = undefined;
      console.log(error);
      this.router.navigate(['/']);
    });
  }

  getGame() {
    this.dataProviderService.getGame()
    .subscribe(result => {
      this.game = result;
      this.initBoard();
      this.getVictory();
    }, error => {
      this.game = undefined;
      console.log(error);
    });
  }

  getVictory() {
    this.dataProviderService.getVictory()
    .subscribe(result => {
      this.isGameFinished = Boolean(result);
    }, error => {
      console.log(error);
    });
  }

  getIsAIPlayer(): Promise<boolean> {
    return new Promise(resolve => {
      this.dataProviderService.getIsAIPlayer()
      .subscribe(result => {
        this.isAIPlayer = Boolean(result);
      }, error => {
        console.log(error);
      }, () => { resolve(true); });
    });
  }

  getAIPlayerMove(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        this.dataProviderService.getAIPlayerMove()
        .subscribe(result => {
          this.game = result;
          this.initBoard();
        }, error => {
          this.game = undefined;
          console.log(error);
        }, () => { resolve(true); });
      }, 1000);
    });
  }

  putPlay(x: number, y: number) {
    this.dataProviderService.putPlay(this.current.x, this.current.y, x, y)
    .subscribe(result => {
      this.game = result;
      this.initBoard();
      this.getVictory();
    }, error => {
      console.log(error);
    });
  }

  putUndo() {
    this.dataProviderService.putUndo()
    .subscribe(result => {
      this.game = result;
      this.initBoard();
    }, error => {
      console.log(error);
    });
  }

  putRedo() {
    this.dataProviderService.putRedo()
    .subscribe(result => {
      this.game = result;
      this.initBoard();
    }, error => {
      console.log(error);
    });
  }

  putEndTurn() {
    this.dataProviderService.putEndTurn()
    .subscribe(async result => {
      this.game = result;
      this.initBoard();
      <boolean>await this.getIsAIPlayer();
      if(this.isAIPlayer) {
        for(let i = 0 ; i < 3 ; i++) {
          <boolean>await this.getAIPlayerMove();
        }
        this.putEndTurn();
      }
    }, error => {
      console.log(error);
    });
  }

  putReset() {
    this.dataProviderService.putReset()
    .subscribe(result => {
      this.game = result;
      this.initBoard();
      this.getVictory();
    }, error => {
      console.log(error);
    });
  }

  initBoard() {
    for(let x = 0 ; x < 7 ; x++) {
      for(let y = 0 ; y < 7 ; y++) {
        this.board[(x * 7) + y] = this.game.board.pieces.find(p => (p.coordinate.x == x && p.coordinate.y == y));
      }
    }
  }

  hasPiece(x: number, y: number): boolean {
    return this.board[(x * 7) + y] != undefined;
  }

  hasBall(x: number, y: number): boolean {
    return this.board[(x * 7) + y].hasBall;
  }

  isDraggable(x: number, y: number): boolean {
    return this.board[(x * 7) + y].player.isWhite == this.game.players[this.game.indexCurrentPlayer].isWhite && !this.isAIPlayer;
  }

  isWhite(x: number, y: number): boolean {
    return this.board[(x * 7) + y].isWhite;
  }

  onDragOver(event: any) {
    event.preventDefault();
  }

  onDropPiece(event: any, x: number, y: number) {
    event.preventDefault();
    if(this.current.piece) {
      if(this.current.piece.hasBall) {
        this.putPlay(x, y);
      }
    }
  }

  onDropEmpty(event: any, x: number, y: number) {
    event.preventDefault();
    if(this.current.piece) {
      this.putPlay(x, y);
    }
  }

  onDragStart(event: any, x: number, y: number) {
    event.dataTransfer.setData('text', '');
    this.current.piece = this.board[(x * 7) + y];
    this.current.x = x;
    this.current.y = y;
  }

  onUndo() {
    this.putUndo();
  }

  onRedo() {
    this.putRedo();
  }

  onEndTurn() {
    if(this.game.nbActions == 3) {
      this.putEndTurn();
    }
  }

  onRetry() {
    this.putReset();
  }

  onRestart() {
    this.router.navigate(['/']);
  }
}
