package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * this class represents the board with a List of pieces
 *
 * @author Guénaël Bournac
 * @version 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Board {

    private List<Piece> pieces;

    private Player p1;

    private Player p2;

    /**
     * Constructor for Board
     * @param p1 player one
     * @param p2 player two
     */
    public Board(final Player p1, final Player p2) {

        this.p1 = p1;
        this.p2 = p2;

        //The player one Pieces are created
        final List<Piece> pieceP1 = IntStream
                .range(0, 7)
                .mapToObj((x) -> new Piece(p1, x, 6))
                .collect(Collectors.toList());

        pieceP1.get(3).setBall(true);

        //The player Two Pieces are created
        final List<Piece> pieceP2 = IntStream
                .range(0, 7)
                .mapToObj((x) -> new Piece(p2, x, 0))
                .collect(Collectors.toList());

        pieceP2.get(3).setBall(true);

        pieces = new ArrayList<>();

        pieces.addAll(pieceP1);
        pieces.addAll(pieceP2);

    }

    private Board() {
        this.p1 = null;
        this.p2 = null;
        this.pieces = null;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public boolean isVictoryAchieved() {
        final long victoryP1 = pieces
                .stream()
                .filter(p -> p.getPlayer() == p1)
                .filter(p -> p.getCoordinate().getY() == 0)
                .filter(p -> p.hasBall())
                .count();

        final long victoryP2 = pieces
                .stream()
                .filter(p -> p.getPlayer() == p2)
                .filter(p -> p.getCoordinate().getY() == 6)
                .filter(p -> p.hasBall())
                .count();

        if(victoryP1 == 1) {
            p1.setWin(true);
        }else if(victoryP2 == 1) {
            p2.setWin(true);
        }

        return victoryP1 == 1 || victoryP2 == 1;
    }
}
