import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { StartGameComponent } from './start-game.component';
import { DataProviderService } from '../../services/data-provider.service';
import { IGame } from 'src/app/interfaces/igame';
import { Observable, of as observableOf } from 'rxjs';

let game: IGame = undefined;

class DataProviderServiceMock extends DataProviderService {
  putReset(): Observable<IGame> {
    return observableOf(game);
  }

  putRedo(): Observable<IGame> {
    return observableOf(game);
  }

  putUndo(): Observable<IGame> {
    return observableOf(game);
  }

  putEndTurn(): Observable<IGame> {
    return observableOf(game);
  }

  putPlay(x1: number, y1: number, x2: number, y2: number): Observable<IGame> {
    return observableOf(game);
  }

  postNewGame(level: number, name1: string, color1: string, name2: string, color2: string): Observable<IGame> {
    return observableOf(game);
  }

  getVictory(): Observable<string> {
    return observableOf("true");
  }

  getGame(): Observable<IGame> {
    return observableOf(game);
  }
}

describe('StartGameComponent', () => {
  let component: StartGameComponent;
  let fixture: ComponentFixture<StartGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StartGameComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [{ provide: DataProviderService, useClass: DataProviderServiceMock }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
