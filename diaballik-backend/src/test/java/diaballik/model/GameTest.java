package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private Game game;
    private Player p1;
    private Player p2;

    @BeforeEach
    void setUp() {
        this.p1 = new Player(true, "p1");
        this.p2 = new Player(false, "p2");
        this.game = new Game(p1, p2);
    }

    @Test
    void gameNullParameter() {
        assertThrows(IllegalArgumentException.class, () -> new Game(null, p2));
    }

    @Test
    void gameSamePlayers() {
        Player p1Bis = new Player(this.p1.isWhite(), this.p1.getName());
        assertThrows(IllegalArgumentException.class, () -> new Game(p1, p1Bis));
    }

    @Test
    void newTurn() {
        assertEquals(this.game.getNbTurn(), 0);
        this.game.newTurn();
        assertEquals(this.game.getNbTurn(), 1);
    }

    @Test
    void getNbTurn() {
        assertEquals(this.game.getNbTurn(), 0);
    }

    @Test
    void getLastTurnException() {
        assertThrows(Exception.class, () -> this.game.getLastTurn());
    }

    @Test
    void getLastTurn() throws Exception {
        this.game.newTurn();
        Turn turn1 = this.game.getLastTurn();
        this.game.newTurn();
        Turn turn2 = this.game.getLastTurn();

        assertEquals(this.game.getNbTurn(), 2);
        // reference comparison
        assertTrue(turn1 != turn2);
    }

    @Test
    void getBoard() {
        // ???
        assertNotNull(this.game.getBoard());
    }

    @Test
    void getNextPlayer() {
        assertEquals(this.game.getNextPlayer(), p1);
        assertEquals(this.game.getNextPlayer(), p2);
        assertEquals(this.game.getNextPlayer(), p1);
    }

    @Test
    void reset() throws Exception {
        Board currentBoard = this.game.getBoard();

        this.game.reset();

        // reference comparison
        assertTrue(currentBoard != this.game.getBoard());
        assertEquals(this.game.getNbTurn(), 0);
        assertThrows(Exception.class, () -> this.game.getLastTurn());
        // reset() call doesn't change the current players though
        List<Player> playersList = this.game.getPlayers();
        assertEquals(playersList.get(0), p1);
        assertEquals(playersList.get(1), p2);
    }

    @Test
    void getPlayers() {
        List<Player> playersList = this.game.getPlayers();
        assertEquals(playersList.get(0), p1);
        assertEquals(playersList.get(1), p2);
    }

    @Test
    void indexNextPlayerBeforePlay() throws NoSuchFieldException, IllegalAccessException {
        Field f = this.game.getClass().getDeclaredField("indexCurrentPlayer");
        f.setAccessible(true);
        int index = (int) f.get(this.game);
        assertEquals(1, index);
    }

    @Test
    void indexNextAfterReset() throws NoSuchFieldException, IllegalAccessException {
        this.game.reset();
        Field f = this.game.getClass().getDeclaredField("indexCurrentPlayer");
        f.setAccessible(true);
        int index = (int) f.get(this.game);
        assertEquals(1, index);
    }

    @Test
    void indexNextPlayerAfterPlay() throws NoSuchFieldException, IllegalAccessException {
        this.game.newTurn();
        Field f = this.game.getClass().getDeclaredField("indexCurrentPlayer");
        f.setAccessible(true);
        int index = (int) f.get(this.game);
        assertEquals(0, index);
    }

    @Test
    void indexNextPlayerAfter2Plays() throws NoSuchFieldException, IllegalAccessException {
        this.game.newTurn();
        this.game.newTurn();
        Field f = this.game.getClass().getDeclaredField("indexCurrentPlayer");
        f.setAccessible(true);
        int index = (int) f.get(this.game);
        assertEquals(1, index);
    }
}