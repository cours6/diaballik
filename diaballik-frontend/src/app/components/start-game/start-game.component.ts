import { Component } from '@angular/core';
import { DataProviderService } from 'src/app/services/data-provider.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css']
})
export class StartGameComponent {

  level: number = undefined;
  n1: string = '';
  n2: string = '';
  c1: boolean = true;

  constructor(private dataProviderService: DataProviderService, private router: Router) { }

  onNewGame() {
    if(this.n1 === '' || this.n2 === '' || this.level === undefined || this.n1 === this.n2) {
      return;
    }

    let color1 = this.c1 ? "white" : "black";
    let color2 = this.c1 ? "black" : "white";
    this.dataProviderService.postNewGame(this.level, this.n1, color1, this.n2, color2)
    .subscribe(result => {
      this.router.navigate(['/play']);
    }, error => {
      console.log(error);
    });
  }

  onCheck() {
    this.c1 = !this.c1;
  }

  onRadio(level: string) {
    // Number('a') returns NaN so we add || 0 for that case
    this.level = Number(level) || 0;
  }
}
