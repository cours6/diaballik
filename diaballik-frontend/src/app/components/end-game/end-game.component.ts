import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-end-game',
  templateUrl: './end-game.component.html',
  styleUrls: ['./end-game.component.css']
})
export class EndGameComponent {
  @Input()
  public name: string = '';
  @Input()
  public turns: number = 0;
  @Output()
  replay = new EventEmitter();
  @Output()
  restart = new EventEmitter();

  constructor() { }

  onReplay() {
    this.replay.emit();
  }

  onRestart() {
    this.restart.emit();
  }
}
