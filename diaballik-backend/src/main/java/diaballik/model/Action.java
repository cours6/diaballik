package diaballik.model;

/**
 * this interface describes a playable action
 *
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public interface Action {

    void doS(Board board);
    boolean canDo(Board board);
}
