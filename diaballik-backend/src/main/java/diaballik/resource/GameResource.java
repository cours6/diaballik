package diaballik.resource;

import diaballik.model.Command;
import diaballik.model.Coordinate;
import diaballik.model.Factory;
import diaballik.model.Game;
import diaballik.model.Player;
import diaballik.model.PlayerAI;
import io.swagger.annotations.Api;
import javax.inject.Singleton;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Singleton
@Path("game")
@Api(value = "game")
public class GameResource {

	Game game;

	public GameResource() {
		super();
		this.game = null;
	}

	/**
	 * PUT game/reset
	 * this request is used to reset the current game.
	 * resetting a game consists in starting a new game with the current players
	 * @return the current game
	 */
	@PUT
	@Path("reset")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putResetGame() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		game.reset();
		game.newTurn();
		return Response.status(Response.Status.OK).entity(game).build();
	}

	/**
	 * PUT game/redo
	 * this request is used to redo a play (which was undone before)
	 * @return the current game
	 */
	@PUT
	@Path("redo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putRedo() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			game.getLastTurn().redo(game.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.status(Response.Status.OK).entity(game).build();
	}

	/**
	 * PUT game/undo
	 * this request is used to undo the last move of the current player
	 * @return the current game
	 */
	@PUT
	@Path("undo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putUndo() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			game.getLastTurn().undo(game.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.status(Response.Status.OK).entity(game).build();
	}

	/**
	 * PUT game/endTurn
	 * this request is used to finish the current turn
	 * if the next player is not human, this makes it play its three moves
	 * @return the current game
	 */
	@PUT
	@Path("endTurn")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putEndTurn() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			if(this.game.getLastTurn().isFinish()) {
				this.game.newTurn();
				return Response.status(Response.Status.OK).entity(game).build();
			} else {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@GET
	@Path("isAIPlayer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIsAIPlayer() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		final boolean isAIPlayer = this.game.getCurrentPlayer() instanceof PlayerAI;
		return Response.status(Response.Status.OK).entity(String.valueOf(isAIPlayer)).build();
	}

	@GET
	@Path("aiPlayerMove")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIsAIPlayerMove() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			final Player p = this.game.getCurrentPlayer();
			if(p instanceof PlayerAI && !this.game.getLastTurn().isFinish()) {
				final PlayerAI player = (PlayerAI) p;
				final Command c1 = player.play(this.game.getBoard(), this.game.getLastTurn().getNbActions() + 1);
				this.game.getLastTurn().play(this.game.getBoard(), c1);
				return Response.status(Response.Status.OK).entity(game).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	/**
	 * PUT game/play/{x1}/{y1}/{x2}/{y2}
	 * this request is used to play : either move a ball or move a piece
 	 * @param x1 current x coordinate of the targeted piece
	 * @param y1 current y coordinate of the targeted piece
	 * @param x2 next x coordinate of the targeted piece
	 * @param y2 next y coordinate of the targeted piece
	 * @return the current game
	 */
	@PUT
	@Path("play/{x1}/{y1}/{x2}/{y2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putPlay(@PathParam("x1") final int x1, @PathParam("y1") final int y1, @PathParam("x2") final int x2, @PathParam("y2") final int y2) {
		if(this.game == null || this.game.getBoard().isVictoryAchieved() || this.game.getCurrentPlayer() instanceof PlayerAI) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		final Coordinate c1 = new Coordinate(x1, y1);
		final Coordinate c2 = new Coordinate(x2, y2);
		final Command c;
		try {
			c = Factory.getINSTANCE().createCommand(this.game.getBoard(), c1, c2, this.game.colorCurrentPlayer());
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		if(c.canDo(this.game.getBoard())) {
			try {
				this.game.getLastTurn().play(this.game.getBoard(), c);
				this.game.getBoard().isVictoryAchieved();
				return Response.status(Response.Status.OK).entity(game).build();
			} catch (Exception e) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}

	/**
	 * POST game/new/{level}/{name1}/{color1}/{name2}/{color2}
	 * this request is used to create a game
	 * @param level describes the nature of the second player (human or AI)
	 * @param name1 first player's name
	 * @param color1 first player's color
	 * @param name2 second player's name
	 * @param color2 second player's color
	 * @return the current game
	 */
	@POST
	@Path("new/{level}/{name1}/{color1}/{name2}/{color2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response postNewGame(@PathParam("level") final int level, @PathParam("name1") final String name1, @PathParam("color1") final String color1, @PathParam("name2") final String name2, @PathParam("color2") final String color2) {
		try {
			final Player p1 = Factory.getINSTANCE().createPlayer(0, name1, color1);
			final Player p2 = Factory.getINSTANCE().createPlayer(level, name2, color2);
			this.game = Factory.getINSTANCE().createGame(p1, p2);
			this.game.newTurn();
			return Response.status(Response.Status.OK).entity(game).build();
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * GET game/victory
	 * this request is used to indicate whether the victory has been achieved by one of the players
	 * @return a boolean-like string ("true" or "false")
	 */
	@GET
	@Path("victory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIsVictoryAchieved() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		final boolean victory = this.game.getBoard().isVictoryAchieved();
		return Response.status(Response.Status.OK).entity(String.valueOf(victory)).build();
	}

	/**
	 * GET game/nbActions
	 * this request is used to know the number of actions already played by the current player
	 * @return an integer-like string ("0", "1", "2", "3")
	 */
	@GET
	@Path("nbActions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNbActions() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			final int nbActions = this.game.getLastTurn().getNbActions();
			return Response.status(Response.Status.OK).entity(String.valueOf(nbActions)).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	/**
	 * GET game/current
	 * this request is used to know the current game
	 * @return the current game
	 */
	@GET
	@Path("current")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCurrentGame() {
		if(this.game == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		try {
			return Response.status(Response.Status.OK).entity(game).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

}
