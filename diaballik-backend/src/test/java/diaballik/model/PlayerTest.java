package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    private Player p1;
    private Player p1Bis;
    private Player p2;

    @BeforeEach
    void setUp() {
        p1 = new Player(true, "player1");
        p1Bis = new Player(true, "player1");
        p2 = new Player(false, "player2");
    }

    @Test
    void playerNullNameException() {
        assertThrows(IllegalArgumentException.class, () -> new Player(true, null));
    }

    @Test
    void playerEmptyNameException() {
        assertThrows(IllegalArgumentException.class, () -> new Player(true, ""));
    }

    @Test
    void isWhitePlayer1() {
        assertTrue(p1.isWhite());
    }

    @Test
    void isWhitePlayer2() {
        assertFalse(p2.isWhite());
    }

    @Test
    void getNamePlayer1() {
        assertEquals(p1.getName(), "player1");
    }

    @Test
    void getNamePlayer2() {
        assertEquals(p2.getName(), "player2");
    }

    @Test
    void getWin() {
        assertFalse(p1.getWin());
    }

    @Test
    void setWin() {
        p1.setWin(true);
        assertTrue(p1.getWin());
    }

    @Test
    void isHumanPlayer() {
        assertTrue(p1.isHumanPlayer());
    }

    @Test
    void equalsFalse() {
        assertNotEquals(p1, p2);
    }

    @Test
    void equalsTrue() {
        assertEquals(p1, p1Bis);
    }
}