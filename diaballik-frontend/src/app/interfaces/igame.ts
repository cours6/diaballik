export interface IPlayer {
    name: string;
    hasWon: boolean;
    isWhite: boolean;
}

export interface ICoordinate {
    x: number;
    y: number;
}

export interface IBoard {
    pieces: IPiece[];
    p1: IPlayer;
    p2: IPlayer;
}

export interface IPiece {
    hasBall: boolean;
    isWhite: boolean;
    player: IPlayer;
    coordinate: ICoordinate;
}

export interface IGame {
    nbTurn: number;
    players: IPlayer[];
    board: IBoard;
    indexCurrentPlayer: number;
    nbActions: number;
}
