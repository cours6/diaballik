package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * this class represents a player
 * @author Quentin Mazouni
 * @version 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Player {

    private String name;
    private boolean hasWon;
    private boolean isWhite;

    /**
     * @param isWhite true if player's color is white
     * @param name player's name
     * @throws IllegalArgumentException if name is null or empty string
     * @since 1.0.0
     */
    public Player(final boolean isWhite, final String name) {
        hasWon = false;
        this.isWhite = isWhite;
        if(name == null || ("").equals(name)) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    private Player() {
        this.hasWon = false;
        this.isWhite = false;
        this.name = null;

    }

    /**
     * @return true if the player's color is white
     * @since 1.0.0
     */
    public boolean isWhite() {
        return isWhite;
    }

    /**
     * @return player's name
     * @since 1.0.0
     */
    public String getName() {
        return name;
    }

    /**
     * @return true if the player's has won the game
     * @since 1.0.0
     */
    public boolean getWin() {
        return hasWon;
    }

    /**
     * @param hasWon boolean value
     * @since 1.0.0
     */
    public void setWin(final boolean hasWon) {
        this.hasWon = hasWon;
    }

    /**
     * @return true if the player is human
     * @since 1.0.0
     */
    public boolean isHumanPlayer() {
        return true;
    }

    /**
     * @param o object instance to be compared with
     * @return true if both players have the same color
     * @since 1.0.0
     */
    @Override
    public boolean equals(final Object o) {
        if(o == null) {
            return false;
        }

        if(o instanceof Player) {
            return this.isWhite == ((Player) o).isWhite;
        } else {
            return false;
        }
    }

    /**
     * @return  hashCode
     * @since 1.0.0
     */
    @Override
    public int hashCode() {
        return Objects.hash(isWhite);
    }
}
