package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class CoordinateTest {

    Coordinate c1Equals;
    Coordinate c2Equals;
    Coordinate c3Equals;

    Coordinate c1RowColumn;

    @BeforeEach
    void setUp() {
        c1Equals = new Coordinate(0,0);
        c2Equals = new Coordinate(0,0);
        c3Equals = new Coordinate(2,2);

        c1RowColumn = new Coordinate(2,2);
    }
    
    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void getX(int x) {
        Coordinate c = new Coordinate(x,0);
        assertEquals(x,c.getX());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void getY(int y) {
        Coordinate c = new Coordinate(0,y);
        assertEquals(y,c.getY());
    }

    @Test
    void equalsSame1() {
        assertEquals(c1Equals, c1Equals);
    }

    @Test
    void equalsSame2() {
        assertEquals(c1Equals, c2Equals);
    }

    @Test
    void equalsDifferent1() {
        assertNotEquals(c1Equals, c3Equals);
    }

    @Test
    void equalsDifferent2() {
        assertNotEquals(c3Equals, c2Equals);
    }

    @Test
    void equalsNull(){
        assertFalse(c1Equals.equals(null));
    }

    @Test
    void equalsObject(){
        assertFalse(c1Equals.equals(new Object()));
    }
    
    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameColumn1(int y) {
        Coordinate c2 = new Coordinate(c1RowColumn.getX(),y);
        assertTrue(c1RowColumn.onSameColumn(c2));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 6, 4})
    void onSameColumn2(int x) {
        Coordinate c2 = new Coordinate(x,0);
        assertFalse(c1RowColumn.onSameColumn(c2));
    }

    @Test
    void onSameColumnNull() {
        assertFalse(c1RowColumn.onSameColumn(null));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameRow1(int x) {
        Coordinate c2 = new Coordinate(x,c1RowColumn.getY());
        assertTrue(c1RowColumn.onSameRow(c2));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 6, 4})
    void onSameRow2(int y) {
        Coordinate c2 = new Coordinate(0,y);
        assertFalse(c1RowColumn.onSameRow(c2));
    }

    @Test
    void onSameRowNull() {
        assertFalse(c1RowColumn.onSameRow(null));
    }

    @Test
    void onSameDiagonal1() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(3,3);
        assertTrue(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal2() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(0,0);
        assertTrue(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal3() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(4,0);
        assertTrue(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal4() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(0,4);
        assertTrue(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal5() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(1,0);
        assertFalse(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal6() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(2,3);
        assertFalse(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonal7() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(5,4);
        assertFalse(c1.onSameDiagonal(c2));
    }

    @Test
    void onSameDiagonalNull() {
        Coordinate c1=new Coordinate(2,2);
        assertFalse(c1.onSameDiagonal(null));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 7, 45})
    void constructorExceptionX(int x){
        assertThrows(IllegalArgumentException.class, ()-> new Coordinate(x, 3));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 7, 45})
    void constructorExceptionY(int y){
        assertThrows(IllegalArgumentException.class, ()-> new Coordinate(3, y));
    }

    @Test
    void isBetweenColumn1(){
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(0,1);
        Coordinate c3 = new Coordinate(0,2);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenColumn2(){
        Coordinate c1 = new Coordinate(2,0);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(2,5);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenColumn3(){
        Coordinate c1 = new Coordinate(2,0);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(2,5);
        assertFalse(c1.isBetween(c2, c3));
        assertFalse(c3.isBetween(c2, c1));
    }

    @Test
    void isBetweenRow1(){
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(1,0);
        Coordinate c3 = new Coordinate(2,0);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenRow2(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(4,2);
        Coordinate c3 = new Coordinate(5,2);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenRow3(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(4,2);
        Coordinate c3 = new Coordinate(5,2);
        assertFalse(c1.isBetween(c2, c3));
        assertFalse(c3.isBetween(c2, c1));
    }

    @Test
    void isBetweenDiagonal1(){
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(2,2);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenDiagonal2(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(2,0);
        assertTrue(c2.isBetween(c1, c3));
        assertTrue(c2.isBetween(c3, c1));
    }

    @Test
    void isBetweenDiagonal3(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(2,0);
        assertFalse(c1.isBetween(c2, c3));
        assertFalse(c3.isBetween(c2, c1));
    }

    @Test
    void isBetweenDiagonal4(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(0,0);
        assertFalse(c2.isBetween(c1, c3));
    }

    @Test
    void isBetweenFalse(){
        Coordinate c1 = new Coordinate(0,2);
        Coordinate c2 = new Coordinate(1,1);
        Coordinate c3 = new Coordinate(0,4);
        assertFalse(c2.isBetween(c1, c3));
    }
}