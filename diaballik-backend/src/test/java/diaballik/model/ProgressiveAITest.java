package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class ProgressiveAITest {

    PlayerAI ai;
    Board b;

    @BeforeEach
    void setup(){
        ai = new PlayerAI(false, "toto", 3);
        Player p = new Player(true, "tata");
        b = new Board(p,ai);
    }

    @Test
    void progressiveAITest() throws NoSuchFieldException, IllegalAccessException {

        Field f = ai.getClass().getDeclaredField("strategy");
        f.setAccessible(true);
        AI ia = (AI) f.get(ai);
        assertTrue(ia instanceof ProgressiveAI);


        ProgressiveAI progressiveai = (ProgressiveAI) ia;
        Field fAI = progressiveai.getClass().getDeclaredField("ai");
        fAI.setAccessible(true);
        AI strategyAI = (AI) fAI.get(ia);
        assertTrue(strategyAI instanceof NoobAI);
    }

    @Test
    void changeAITest() throws NoSuchFieldException, IllegalAccessException {

        ai.play(b,1);
        ai.play(b,1);
        ai.play(b,1);

        ai.play(b,1);
        ai.play(b,1);
        ai.play(b,1);

        ai.play(b,1);
        ai.play(b,1);
        ai.play(b,1);

        ai.play(b,1);
        ai.play(b,1);
        ai.play(b,1);
        
        Field f = ai.getClass().getDeclaredField("strategy");
        f.setAccessible(true);
        AI ia = (AI) f.get(ai);
        assertTrue(ia instanceof ProgressiveAI);


        ProgressiveAI progressiveai = (ProgressiveAI) ia;
        Field fAI = progressiveai.getClass().getDeclaredField("ai");
        fAI.setAccessible(true);
        AI strategyAI = (AI) fAI.get(ia);
        assertTrue(strategyAI instanceof NoobAI);

        ai.play(b,1);

        f = ai.getClass().getDeclaredField("strategy");
        f.setAccessible(true);
        ia = (AI) f.get(ai);
        assertTrue(ia instanceof ProgressiveAI);


        progressiveai = (ProgressiveAI) ia;
        fAI = progressiveai.getClass().getDeclaredField("ai");
        fAI.setAccessible(true);
        strategyAI = (AI) fAI.get(ia);
        assertTrue(strategyAI instanceof StartingAI);
    }

}