import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DataProviderService } from './services/data-provider.service';
import { Observable, of as observableOf } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IGame } from './interfaces/igame';

let game: IGame = {
    'nbTurn': 1,
    'players': [
        {
            'name': 'a',
            'hasWon': false,
            'isWhite': true
        },
        {
            'name': 'b',
            'hasWon': false,
            'isWhite': false
        }
    ],
    'board': {
        'pieces': [
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 0,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 1,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 2,
                    'y': 6
                }
            },
            {
                'hasBall': true,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 3,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 4,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 5,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': true,
                'player': {
                    'name': 'a',
                    'hasWon': false,
                    'isWhite': true
                },
                'coordinate': {
                    'x': 6,
                    'y': 6
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 0,
                    'y': 0
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 1,
                    'y': 0
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 2,
                    'y': 0
                }
            },
            {
                'hasBall': true,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 3,
                    'y': 0
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 4,
                    'y': 0
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 5,
                    'y': 0
                }
            },
            {
                'hasBall': false,
                'isWhite': false,
                'player': {
                    'name': 'b',
                    'hasWon': false,
                    'isWhite': false
                },
                'coordinate': {
                    'x': 6,
                    'y': 0
                }
            }
        ],
        'p1': {
            'name': 'a',
            'hasWon': false,
            'isWhite': true
        },
        'p2': {
            'name': 'b',
            'hasWon': false,
            'isWhite': false
        }
    },
    'indexCurrentPlayer': 0,
    'nbActions': 0
};


class DataProviderServiceMock extends DataProviderService {

    putReset(): Observable<IGame> {
        return observableOf(game);
    }

    postNewGame(level: number, name1: string, color1: string, name2: string, color2: string): Observable<IGame> {
        return observableOf(game);
    }
}

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientModule
            ],
            declarations: [
                AppComponent
            ],
            providers: [
                { provide: DataProviderService, useClass: DataProviderServiceMock }
            ]
        }).compileComponents();
    }));

    it('should create', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
