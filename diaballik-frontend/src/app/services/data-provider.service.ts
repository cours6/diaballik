import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IGame } from '../interfaces/igame';

@Injectable()
export class DataProviderService {

    constructor(private http: HttpClient) { }

    putReset(): Observable<IGame> {
        return this.http.put<IGame>('/game/reset', { });
    }

    putRedo(): Observable<IGame> {
        return this.http.put<IGame>('/game/redo', { });
    }

    putUndo(): Observable<IGame> {
        return this.http.put<IGame>('/game/undo', { });
    }

    putEndTurn(): Observable<IGame> {
        return this.http.put<IGame>('/game/endTurn', { });
    }

    putPlay(x1: number, y1: number, x2: number, y2: number): Observable<IGame> {
        return this.http.put<IGame>(`/game/play/${x1}/${y1}/${x2}/${y2}`, { });
    }

    postNewGame(level: number, name1: string, color1: string, name2: string, color2: string): Observable<IGame> {
        return this.http.post<IGame>(`/game/new/${level}/${name1}/${color1}/${name2}/${color2}`, { });
    }

    getVictory(): Observable<string> {
        return this.http.get<string>('/game/victory');
    }

    getNbActions(): Observable<string> {
        return this.http.get<string>('/game/nbActions');
    }

    getGame(): Observable<IGame> {
        return this.http.get<IGame>('/game/current');
    }

    getIsAIPlayer(): Observable<string> {
        return this.http.get<string>('/game/isAIPlayer');
    }

    getAIPlayerMove(): Observable<IGame> {
        return this.http.get<IGame>('/game/aiPlayerMove');
    }
}
