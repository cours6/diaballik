import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartGameComponent } from './components/start-game/start-game.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { PlayGameComponent } from './components/play-game/play-game.component';


const routes: Routes = [
    { path: 'play', component: PlayGameComponent },
    { path: 'start', component: StartGameComponent },
    { path: '', redirectTo: '/start', pathMatch: 'full' },
    { path: '**', component: NotFoundPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
