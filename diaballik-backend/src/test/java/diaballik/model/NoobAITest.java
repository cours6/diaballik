package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoobAITest {

    private Player p1;
    private Player p2;
    private Board board;
    private NoobAI ai;

    @BeforeEach
    void setUp() {
        this.p1 = new Player(true, "p1");
        this.p2 = new Player(false, "p2");
        this.board = new Board(this.p1, this.p2);
        this.ai = new NoobAI();
    }

    @Test
    void playNullBoardException() {
        assertThrows(IllegalArgumentException.class, () -> this.ai.play(null, 3, false));
    }

    @RepeatedTest(20)
    void play() {
        Command command = this.ai.play(this.board, 3, false);
        Coordinate oldC = command.getOldCoordinate();
        boolean isWhite = command.isPlayerIsWhite();
        Piece piece = this.board.getPieces().stream().filter(e -> e.getCoordinate().equals(oldC)).findFirst().get();

        // ai plays for the right player
        assertFalse(isWhite);
        // the piece associated to the move belongs to the right player
        assertFalse(piece.isWhite());
        // the computed command can be played
        assertTrue(command.canDo(this.board));
    }
}