package diaballik.model;

import java.util.List;
import java.util.stream.Collectors;

/**
 * this class represents a starting level AI
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public class StartingAI extends AI {


    NoobAI ai;

    /**
     * makes the ai play a move
     * @param board current board of the game
     * @param nbActions number of actions left to be played during the current turn
     * @param isPlayerWhite player's color for whom the AI plays
     * @return a command which describes the ai's move
     * @throws IllegalArgumentException if the board is null
     * @since 1.0.0
     */
    @Override
    public Command play(final Board board, final int nbActions, final boolean isPlayerWhite) {
        if(board == null) {
            throw new IllegalArgumentException();
        }
        
        final Command command;
        final List<Piece> pieces = board.getPieces().stream().filter(e -> e.isWhite() == isPlayerWhite).collect(Collectors.toList());
        final Piece piece = pieces.get((int) (Math.random() * pieces.size()));


        if(piece.hasBall()) {
            // if the randomly chosen piece has the ball, then we try to give it to one of the remaining pieces...
            pieces.remove(piece);
            command = new MoveBall(piece.getCoordinate(), pieces.get((int) (Math.random() * pieces.size())).getCoordinate(), isPlayerWhite);
        } else {
            final Piece pieceAdversaryBall = board.getPieces()
                    .stream()
                    .filter(e -> e.isWhite() != isPlayerWhite)
                    .filter(e -> e.hasBall())
                    .findFirst().get();

            command = tryMovePiece(board, nbActions, isPlayerWhite, piece, pieceAdversaryBall);
        }
        
        return command.canDo(board) ? command : this.play(board, nbActions, isPlayerWhite);
    }

    /**
     * tries to move the piece
     * @param board current board od the game
     * @param nbActions current number of actions already played
     * @param isPlayerWhite true if the player's color is white
     * @param piece piece to be moved
     * @param pieceAdversaryBall adversary's piece which has the ball
     * @return command to play
     * @since 1.0.0
     */
    private Command tryMovePiece(final Board board, final int nbActions, final boolean isPlayerWhite, final Piece piece, final Piece pieceAdversaryBall) {
        if(!this.areAdjacent(piece, pieceAdversaryBall)) {
            return tryMovePieceX(board, nbActions, isPlayerWhite, piece, pieceAdversaryBall);
        } else {
            return this.play(board, nbActions, isPlayerWhite);
        }
    }

    /**
     * tries to move the piece on the x axis
     * @param board current board od the game
     * @param nbActions current number of actions already played
     * @param isPlayerWhite true if the player's color is white
     * @param piece piece to be moved
     * @param pieceAdversaryBall adversary's piece which has the ball
     * @return command to play
     * @since 1.0.0
     */
    private Command tryMovePieceX(final Board board, final int nbActions, final boolean isPlayerWhite, final Piece piece, final Piece pieceAdversaryBall) {
        final Command command;
        if(piece.getCoordinate().getX() < pieceAdversaryBall.getCoordinate().getX()) {
            if(board.getPieces().stream()
                    .noneMatch(p -> p.getCoordinate().getX() == piece.getCoordinate().getX() + 1
                            && p.getCoordinate().getY() == piece.getCoordinate().getY())) {
                final Coordinate c1 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY());
                final Coordinate c2 = new Coordinate(piece.getCoordinate().getX() + 1, piece.getCoordinate().getY());
                command =  new MovePiece(c1, c2, isPlayerWhite);
            } else {
                command = tryMovePieceY(board, nbActions, isPlayerWhite, piece, pieceAdversaryBall);
            }
        } else if(piece.getCoordinate().getX() > pieceAdversaryBall.getCoordinate().getX()) {

            if(board.getPieces().stream()
                    .noneMatch(p -> p.getCoordinate().getX() == piece.getCoordinate().getX() - 1
                            && p.getCoordinate().getY() == piece.getCoordinate().getY())) {

                final Coordinate c1 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY());
                final Coordinate c2 = new Coordinate(piece.getCoordinate().getX() - 1, piece.getCoordinate().getY());
                command =  new MovePiece(c1, c2, isPlayerWhite);
            } else {
                command = tryMovePieceY(board, nbActions, isPlayerWhite, piece, pieceAdversaryBall);
            }
        } else {
            command = tryMovePieceY(board, nbActions, isPlayerWhite, piece, pieceAdversaryBall);

        }
        return command;
    }

    /**
     * tries to move the piece on the y axis
     * @param board current board od the game
     * @param nbActions current number of actions already played
     * @param isPlayerWhite true if the player's color is white
     * @param piece piece to be moved
     * @param pieceAdversaryBall adversary's piece which has the ball
     * @return command to play
     * @since 1.0.0
     */
    private Command tryMovePieceY(final Board board, final int nbActions, final boolean isPlayerWhite, final Piece piece, final Piece pieceAdversaryBall) {
        final Command command;
        if(piece.getCoordinate().getY() < pieceAdversaryBall.getCoordinate().getY() &&
                board.getPieces().stream()
                        .noneMatch(p -> p.getCoordinate().getY() == piece.getCoordinate().getY() + 1
                                && p.getCoordinate().getX() == piece.getCoordinate().getX())) {
            
            final Coordinate c1 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY());
            final Coordinate c2 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY() + 1);
            command =  new MovePiece(c1, c2, isPlayerWhite);

        } else if(piece.getCoordinate().getY() > pieceAdversaryBall.getCoordinate().getY() &&
                board.getPieces().stream()
                        .noneMatch(p -> p.getCoordinate().getY() == piece.getCoordinate().getY() - 1
                                && p.getCoordinate().getX() == piece.getCoordinate().getX())) {

            final Coordinate c1 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY());
            final Coordinate c2 = new Coordinate(piece.getCoordinate().getX(), piece.getCoordinate().getY() - 1);
            command =  new MovePiece(c1, c2, isPlayerWhite);

        } else {
            command = null;
        }

        return (command != null) ? command : this.ai.play(board, nbActions, isPlayerWhite);
    }

    /**
     * @param p1 first piece
     * @param p2 second piece
     * @return true if the two pieces in parameters are adjacent
     */
    private boolean areAdjacent(final Piece p1, final Piece p2) {
        final boolean onSameAdjacentRow = p1.getCoordinate().onSameRow(p2.getCoordinate())
                && Math.abs(p1.getCoordinate().getX() - p2.getCoordinate().getX()) == 1;

        final boolean onSameAdjacentColumn = p1.getCoordinate().onSameColumn(p2.getCoordinate())
                && Math.abs(p1.getCoordinate().getY() - p2.getCoordinate().getY()) == 1;

        return (onSameAdjacentRow || onSameAdjacentColumn);
    }

    /**
     * Constructor for StartingAI
     */
    public StartingAI() {
        super();
        this.ai = new NoobAI();
    }
}
