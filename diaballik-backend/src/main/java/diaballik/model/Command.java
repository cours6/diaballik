package diaballik.model;

/**
 * this class represents a command
 * @author Guénaël Bouurnac
 * @version 1.0.0
 */
public abstract class Command implements Action, Undoable {

    /**
     * Old Coordinate of the piece or ball to move
     */
    private Coordinate oldCoordinate;

    /**
     * New Coordinate of the piece or ball to move
     */
    private Coordinate newCoordinate;

    /**
     * Color of the piece owner
     */
    private boolean playerIsWhite;

    /**
     * Constructor based on Coordinates and color player
     * @param oldC Old Coordinate of the piece or ball to move
     * @param newC New Coordinate of the piece or ball to move
     * @param isWhite Color of the piece owner
     * @throws IllegalArgumentException if one of the coordinates is null
     * @since 1.0.0
     */
    public Command(final Coordinate oldC, final Coordinate newC, final boolean isWhite) {
        if(oldC == null || newC == null) {
            throw new IllegalArgumentException();
        }
        this.newCoordinate = newC;
        this.oldCoordinate = oldC;
        this.playerIsWhite = isWhite;
    }

    public Coordinate getOldCoordinate() {
        return oldCoordinate;
    }

    public Coordinate getNewCoordinate() {
        return newCoordinate;
    }

    public boolean isPlayerIsWhite() {
        return playerIsWhite;
    }
}
