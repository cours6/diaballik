import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EndGameComponent } from './end-game.component';
import { OnInit, Component } from '@angular/core';

let moreContent = '<p>test</p>';

describe('EndGameComponent', () => {
  let component: EndGameComponent;
  let fixture: ComponentFixture<EndGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndGameComponent, HostComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  @Component({
    selector: 'host-component',
    template: `<app-end-game
      [name]="nameValue"
      [turns]="turns"
      (replay)="onReplay()"
      (restart)="onRestart()">
        ${moreContent}
      </app-end-game>`
  })
  class HostComponent implements OnInit {
    nameValue: string = '';
    turns: number = 0;

    constructor() { }

    ngOnInit() {
      this.nameValue = 'test name value';
      this.turns = 5;
    }

    onReplay() { }
    onRestart() { }
  }

  it('should create', (done: DoneFn) => {
    fixture.whenStable().then(() => {
      expect(component).toBeTruthy();
      done();
    });
  });
});
