package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * this class represents a game
 * @author Mazouni Quentin
 * @version 1.2.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Game {

    private int nbTurn;

    private List<Player> players;

    @XmlTransient
    private List<Turn> turns;

    private Board board;

    @XmlElement(name = "nbActions")
    public int getNbActions() throws Exception {
        return getLastTurn().getNbActions();
    }

    /**
     * This attribute is used to return the player to play
     * @since 1.1.0
     */
    private int indexCurrentPlayer;

    /**
     * Constructor for Game
     * @param p1 the first player
     * @param p2 the second player
     * @since 1.0.0
     * @throws IllegalArgumentException if one of the parameters is null
     * if both players are the same
     */
    public Game(final Player p1, final Player p2) {
        if(p1 == null || p2 == null || p1.equals(p2)) {
            throw new IllegalArgumentException();
        }

        this.players = new ArrayList<>();
        this.players.add(p1);
        this.players.add(p2);

        this.turns = new ArrayList<>();

        this.board = new Board(p1, p2);
        this.nbTurn = 0;
        this.indexCurrentPlayer = 1;
    }

    private Game() {
        this.nbTurn = 0;
        this.players = null;
        this.indexCurrentPlayer = 1;
        this.board = null;
        this.turns = null;
    }


    /**
     * starts a new turn
     * @since 1.0.0
     */
    public void newTurn() {
        this.getNextPlayer();
        this.turns.add(new Turn(++this.nbTurn));
    }

    /**
     * @return the value of nbTurn
     * @since 1.0.0
     */
    public int getNbTurn() {
        return this.nbTurn;
    }

    /**
     * @return the last Turn
     * @throws Exception if the game has no turn to return
     * @since 1.0.0
     */
    public Turn getLastTurn() throws Exception {
        if(this.nbTurn == 0) {
            throw new Exception();
        } else {
            return this.turns.get(this.nbTurn - 1);
        }
    }

    /**
     * @return the Board of the Game
     * @since 1.0.0
     */
    public Board getBoard() {
        return this.board;
    }

    /**
     * @return the player who will have to play next
     * @since 1.0.0
     */
    public Player getNextPlayer() {
        this.indexCurrentPlayer = (this.indexCurrentPlayer + 1) % 2;

        return this.players.get(this.indexCurrentPlayer);
    }

    /**
     * @return the color of the current player
     * @since 1.2.0
     */
    public boolean colorCurrentPlayer() {
        return this.players.get(this.indexCurrentPlayer).isWhite();
    }

    /**
     * @return the current player
     * @since 1.2.0
     */
    public Player getCurrentPlayer() {
        return this.players.get(this.indexCurrentPlayer);
    }

    /**
     * resets the current game.
     * both players are the same than before
     * but the turns, the board and the number of turns are back to default
     * @since 1.0.0
     */
    public void reset() {
        this.nbTurn = 0;
        this.indexCurrentPlayer = 1;
        this.board = new Board(this.players.get(0), this.players.get(1));
        this.turns = new ArrayList<>();
    }

    /**
     * @return the list of the game's players
     * @since 1.0.0
     */
    public List<Player> getPlayers() {
        return this.players;
    }
}
