package diaballik.resource;

import com.github.hanleyt.JerseyExtension;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import diaballik.model.Coordinate;
import diaballik.model.Game;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.junit.jupiter.api.Assertions.*;

public class TestGameResource {
	static final Logger log = Logger.getLogger(TestGameResource.class.getSimpleName());

	@SuppressWarnings("unused") @RegisterExtension JerseyExtension jerseyExtension = new JerseyExtension(this::configureJersey);

	Application configureJersey() {
		return new ResourceConfig(GameResource.class)
			.register(MyExceptionMapper.class)
			.register(MoxyJsonFeature.class);
	}

	<T> T LogJSONAndUnmarshallValue(final Response res, final Class<T> classToRead) {
		res.bufferEntity();
		final String json = res.readEntity(String.class);
		log.log(Level.INFO, "JSON received: " + json);
		final T obj = res.readEntity(classToRead);
		res.close();
		return obj;
	}

	@BeforeEach
	void setUp(final Client client, final URI baseUri) {
	}

	@Test
	void testNewGamePvP(final Client client, final URI baseUri) {
		 final Response res = client
		 	.target(baseUri)
		 	.path("game/new/0/Toto/white/Tata/black")
		 	.request()
		 	.post(Entity.text(""));

		 assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

		 final Game game = LogJSONAndUnmarshallValue(res, Game.class);
		 assertNotNull(game);

		 assertEquals(2, game.getPlayers().size());
		 assertEquals(14, game.getBoard().getPieces().size());

		 assertEquals("Toto", game.getPlayers().get(0).getName());
		 assertTrue(game.getPlayers().get(0).isWhite());
		 assertEquals("Tata", game.getPlayers().get(1).getName());
		 assertFalse(game.getPlayers().get(1).isWhite());

	}

	@Test
	void testNewGamePvPErrorColor1(final Client client, final URI baseUri) {
		final Response res = client
				.target(baseUri)
				.path("game/new/0/Toto/red/Tata/white")
				.request()
				.post(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res.getStatus());
	}

	@Test
	void testNewGamePvPErrorColor2(final Client client, final URI baseUri) {
		final Response res = client
				.target(baseUri)
				.path("game/new/0/Toto/white/Tata/red")
				.request()
				.post(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res.getStatus());
	}

	@Test
	void testPlayPiece(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));


		final Response res = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
				.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 5)))
		);
	}

	@Test
	void test3PlayPiece(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res3, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 3)))
		);
	}

	@Test
	void test4PlayPiece(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/play/0/3/0/2")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res4.getStatus());
	}

	@Test
	void testPlayBall(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res = client
				.target(baseUri)
				.path("game/play/3/6/2/6")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.filter(p -> p.hasBall())
						.filter(p -> p.isWhite())
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(2, 6)))
		);
		
		assertTrue(
				game.getBoard().getPieces().stream()
						.filter(p -> p.hasBall())
						.filter(p -> p.isWhite())
						.noneMatch(p -> p.getCoordinate().equals(new Coordinate(3, 6)))
		);
	}

	@Test
	void testPlayAdversaryPiece(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res = client
				.target(baseUri)
				.path("game/play/0/0/0/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), res.getStatus());
	}

	@Test
	void testPlayForbiddenMove(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res = client
				.target(baseUri)
				.path("game/play/0/6/1/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), res.getStatus());
	}

	@Test
	void testPlayNoPiece(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res.getStatus());
	}

	@Test
	void test1Undo(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res2, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 6)))
		);
	}

	@Test
	void test1UndoFalse(final Client client, final URI baseUri){

		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));


		final Response res = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res.getStatus());
	}

	@Test
	void test3undo(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res6, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 6)))
		);
	}

	@Test
	void test3undo1redo(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Response res7 = client
				.target(baseUri)
				.path("game/redo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res7.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res7, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 5)))
		);
	}

	@Test
	void test3undo1redoPlay(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Response res7 = client
				.target(baseUri)
				.path("game/redo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res7.getStatus());

		final Response res8 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res8.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res8, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
						.anyMatch(p -> p.getCoordinate().equals(new Coordinate(0, 4)))
		);

		final Response res9 = client
				.target(baseUri)
				.path("game/redo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res9.getStatus());
	}

	@Test
	void testEndTurnFalse(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));


		final Response res = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res.getStatus());
	}

	@Test
	void testEndTurnPvP(final Client client, final URI baseUri){

		final Response res = client
				.target(baseUri)
				.path("game/new/0/Toto/white/Tata/black")
				.request()
				.post(Entity.text(""));
		assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());


		final Response resEnd1 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd1.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/play/1/0/1/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/play/1/1/1/2")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/play/1/2/1/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Response resEnd2 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd2.getStatus());

	}

	@Test
	void testEndTurnPvAI(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/0/6/0/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/0/5/0/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/0/4/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());


		final Response resEnd = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/aiPlayerMove")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/aiPlayerMove")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/aiPlayerMove")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Response resEnd2 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd2.getStatus());

		final Game game = LogJSONAndUnmarshallValue(resEnd2, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces().stream()
				.filter(p -> !p.isWhite())
				.anyMatch(p -> (p.hasBall() && p.getCoordinate().getX() != 3)
				|| p.getCoordinate().getY() != 0)
		);

	}

	@Test
	void testVictory(final Client client, final URI baseUri){

		final Response res = client
				.target(baseUri)
				.path("game/new/0/Toto/white/Tata/black")
				.request()
				.post(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

		final Response res1 = client
				.target(baseUri)
				.path("game/play/2/6/2/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/play/2/5/2/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/play/2/4/2/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response resEnd = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd.getStatus());

		//nextTurn

		final Response res4 = client
				.target(baseUri)
				.path("game/play/3/0/2/0")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/play/3/0/3/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/play/3/1/4/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final Response resEnd2 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd2.getStatus());

		//Turn player one

		final Response res7 = client
				.target(baseUri)
				.path("game/play/2/3/2/2")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res7.getStatus());

		final Response res8 = client
				.target(baseUri)
				.path("game/play/2/2/2/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res8.getStatus());

		final Response res9 = client
				.target(baseUri)
				.path("game/play/2/1/3/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res9.getStatus());

		final Response resEnd3 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd3.getStatus());

		//Turn player two

		final Response res10 = client
				.target(baseUri)
				.path("game/play/0/0/0/1")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res10.getStatus());

		final Response res11 = client
				.target(baseUri)
				.path("game/play/0/1/0/2")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res11.getStatus());

		final Response res12 = client
				.target(baseUri)
				.path("game/play/0/2/0/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res12.getStatus());

		final Response resEnd4 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), resEnd4.getStatus());

		final Response res13 = client
				.target(baseUri)
				.path("game/play/3/1/3/0")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res13.getStatus());

		final Game game1 = LogJSONAndUnmarshallValue(res13, Game.class);
		assertNotNull(game1);

		assertFalse(game1.getPlayers().get(0).getWin());
		assertFalse(game1.getPlayers().get(1).getWin());

		final Response res17 = client
				.target(baseUri)
				.path("game/victory")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res17.getStatus());

		final String victory = LogJSONAndUnmarshallValue(res17, String.class);
		assertNotNull(victory);

		assertEquals("false", victory);

		final Response res14 = client
				.target(baseUri)
				.path("game/play/3/6/3/0")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res14.getStatus());

		final Game game2 = LogJSONAndUnmarshallValue(res14, Game.class);
		assertNotNull(game2);

		final Response res15 = client
				.target(baseUri)
				.path("game/play/6/0/5/0")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res15.getStatus());

		final Response res16 = client
				.target(baseUri)
				.path("game/victory")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res16.getStatus());

		final String victory2 = LogJSONAndUnmarshallValue(res16, String.class);
		assertNotNull(victory2);

		assertEquals("true", victory2);

		assertTrue(game2.getPlayers().get(0).getWin());
		assertFalse(game2.getPlayers().get(1).getWin());
	}

	@Test
	void testNbActions(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res4 = client
				.target(baseUri)
				.path("game/nbActions")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res4.getStatus());

		final String nbActions = LogJSONAndUnmarshallValue(res4, String.class);
		assertNotNull(nbActions);

		assertEquals("0", nbActions);

		final Response res1 = client
				.target(baseUri)
				.path("game/play/2/6/2/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/nbActions")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res5.getStatus());

		final String nbActions2 = LogJSONAndUnmarshallValue(res5, String.class);
		assertNotNull(nbActions2);

		assertEquals("1", nbActions2);

		final Response res2 = client
				.target(baseUri)
				.path("game/play/2/5/2/4")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/nbActions")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res6.getStatus());

		final String nbActions3 = LogJSONAndUnmarshallValue(res6, String.class);
		assertNotNull(nbActions3);

		assertEquals("2", nbActions3);

		final Response res3 = client
				.target(baseUri)
				.path("game/play/2/4/2/3")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res3.getStatus());

		final Response res7 = client
				.target(baseUri)
				.path("game/nbActions")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res7.getStatus());

		final String nbActions4 = LogJSONAndUnmarshallValue(res7, String.class);
		assertNotNull(nbActions4);

		assertEquals("3", nbActions4);
	}

	@Test
	void testReset(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/play/2/6/2/5")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/reset")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

		final Game game = LogJSONAndUnmarshallValue(res2, Game.class);
		assertNotNull(game);

		assertTrue(
				game.getBoard().getPieces()
						.stream()
						.allMatch(p -> p.getCoordinate().getY() == 6 || p.getCoordinate().getY() == 0)
		);

		assertEquals(1, game.getNbTurn());

		assertEquals(game.getPlayers().get(0), game.getCurrentPlayer());
	}

	@Test
	void testException(final Client client, final URI baseUri){
		final Response res1 = client
				.target(baseUri)
				.path("game/play/6/0/5/0")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res1.getStatus());

		final Response res2 = client
				.target(baseUri)
				.path("game/reset")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res2.getStatus());

		final Response res3 = client
				.target(baseUri)
				.path("game/redo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res3.getStatus());

		final Response res4 = client
				.target(baseUri)
				.path("game/undo")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res4.getStatus());

		final Response res5 = client
				.target(baseUri)
				.path("game/endTurn")
				.request()
				.put(Entity.text(""));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res5.getStatus());

		final Response res6 = client
				.target(baseUri)
				.path("game/victory")
				.request()
				.get();

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res6.getStatus());

		final Response res7 = client
				.target(baseUri)
				.path("game/nbActions")
				.request()
				.get();

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res7.getStatus());
	}

	@Test
	void testCurrentGame(final Client client, final URI baseUri){
		client
				.target(baseUri)
				.path("game/new/1/Toto/white/AI/black")
				.request()
				.post(Entity.text(""));

		final Response res1 = client
				.target(baseUri)
				.path("game/current")
				.request()
				.get();

		assertEquals(Response.Status.OK.getStatusCode(), res1.getStatus());
	}

	@Test
	void testCurrentGameNull(final Client client, final URI baseUri){
		final Response res1 = client
				.target(baseUri)
				.path("game/current")
				.request()
				.get();

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), res1.getStatus());
	}
}
