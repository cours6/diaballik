package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Objects;

/**
* This class represents the coordinates of a piece
* the coordinate system is:
* ----> x
* |
* |
* |
* \/
* y
* @author Guénaël Bournac
* @version 1.0.1
* */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Coordinate {

    @XmlTransient
    private static final int MAX = 6;

    /**
    * X coordinate
    * */
    private int x;
    
    /**
     * Y coordinate
     * */
    private int y;

    /**
     * Constructor for Coordinate
     * @param x X coordinate
     * @param y Y coordinate
     * @throws IllegalArgumentException if x or y is lower than zero or greater than Max
     * @since 1.0.1
     */
    public Coordinate(final int x, final int y) {
        if(x < 0 || y < 0 || x > MAX || y > MAX) {
            throw new IllegalArgumentException();
        }
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor to marshall
     */
    private Coordinate() {
        this.x = 0;
        this.y = 0;
    }


    /**
     * getter for x
     * @return X value
     * @since 1.0.0
     */
    public int getX() {
        return x;
    }


    /**
     * getter for y
     * @return Y value
     * @since 1.0.0
     */
    public int getY() {
        return y;
    }

    /**
     * Equals function to compare Coordinates
     * @param o an object to compare with this
     * @return true if objects are equal
     * @since 1.0.0
     */
    @Override
    public boolean equals(final  Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Coordinate)) {
            return false;
        }
        final Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }


    /**
     * @return the hashcode of the coordinate
     * @since 1.0.0
     */
    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    /**
     * Compares two Coordinates to know if they are on the same Row ie have the same y value
     * @param c Coordinate to compare
     * @return true if X value are equal
     * @since 1.0.0
     */
    public boolean onSameRow(final Coordinate c) {
        if(c == null) {
            return false;
        }
        return this.y == c.y;
    }

    /**
     * Compares two Coordinates to know if they are on the same Column ie have the same x value
     * @param c Coordinate to compare
     * @return true if Y value are equal
     * @since 1.0.0
     */
    public boolean onSameColumn(final Coordinate c) {
        if(c == null) {
            return false;
        }
        return this.x == c.x;
    }

    /**
     * Compares two Coordinates to know if they are on the same diagonal ie if the X difference is equal to the Y difference
     * or if the X difference is equal to minus the Y difference
     * @param c Coordinate to compare
     * @return true if they are on the same diagonal
     * @since 1.0.0
     */
    public boolean onSameDiagonal(final Coordinate c) {
        if(c == null) {
            return false;
        }

        final int dx = this.x - c.x;
        final int dy = this.y - c.y;

        return dx == dy || dx == -dy;
    }

    /**
     * this function is used to test if the coordinate is between the given coordinates
     * @param c1 first Coordinate
     * @param c2 second Coordinate
     * @return true if the coordinate is between the Coordinates in parameters
     */
    public boolean isBetween(final Coordinate c1, final Coordinate c2) {
        return isBetweenRow(c1, c2) || isBetweenColumn(c1, c2) || isBetweenDiagonal(c1, c2);
    }

    /**
     * this function is used to test if the coordinate is between the Coordinates in parameters in Row
     * @param c1 first Coordinate
     * @param c2 second Coordinate
     * @return true if the coordinate is between the Coordinates in parameters in Row
     */
    private boolean isBetweenRow(final Coordinate c1, final Coordinate c2) {
        if(this.onSameRow(c1) && this.onSameRow(c2)) {
            return (this.x > c1.x && this.x < c2.x) || (this.x < c1.x && this.x > c2.x);
        }
        return false;
    }

    /**
     * this function is used to test if the coordinate is between the Coordinates in parameters in Column
     * @param c1 first Coordinate
     * @param c2 second Coordinate
     * @return true if the coordinate is between the Coordinates in parameters in Column
     */
    private boolean isBetweenColumn(final Coordinate c1, final Coordinate c2) {
        if(this.onSameColumn(c1) && this.onSameColumn(c2)) {
            return (this.y > c1.y && this.y < c2.y) || (this.y < c1.y && this.y > c2.y);
        }
        return false;
    }

    /**
     * this function tests if the coordinate is between the Coordinates in parameters in Diagonal
     * @param c1 first Coordinate
     * @param c2 second Coordinate
     * @return true if the coordinate is between the Coordinates in parameters in Diagonal
     */
    private boolean isBetweenDiagonal(final Coordinate c1, final Coordinate c2) {
        if(this.onSameDiagonal(c1) && this.onSameDiagonal(c2) && c1.onSameDiagonal(c2)) {
            return (this.x > c1.x && this.x < c2.x) || (this.x < c1.x && this.x > c2.x);
        }
        return false;
    }

    /**
     * To string to display Coordinate in LOG
     * @return
     */
    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

}
