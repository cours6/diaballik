package diaballik.model;

import java.util.Optional;

/**
 * this class represents the player action of moving the ball
 * @author Quentin Mazouni
 * @version 1.0.0
 */
public class MoveBall extends Command {

    /**
     * @param oldC current coordinate of the ball
     * @param newC next coordinate for the ball
     * @param isWhite player's color
     */
    public MoveBall(final Coordinate oldC, final Coordinate newC, final boolean isWhite) {
        super(oldC, newC, isWhite);
    }

    /**
     * this function updates the board wrt the command
     * @param board current board of the game
     * @throws IllegalArgumentException if the command can not be done
     */
    @Override
    public void doS(final Board board) {
        if(this.canDo(board)) {
            board.getPieces()
                    .stream()
                    .filter(p -> p.getCoordinate().equals(super.getOldCoordinate()))
                    .findFirst()
                    .get().
                    setBall(false);

            board.getPieces()
                    .stream()
                    .filter(p -> p.getCoordinate().equals(super.getNewCoordinate()))
                    .findFirst()
                    .get().
                    setBall(true);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * determines whether the command's move is possible (or not)
     * @param board current board of the game
     * @return true if the move is possible
     * @throws IllegalArgumentException if the parameter is null
     * @since 1.0.0
     */
    @Override
    public boolean canDo(final Board board) {
        if(board == null) {
            throw new IllegalArgumentException();
        }

        final Optional<Piece> p1 = board.getPieces()
                .stream()
                .filter(e -> e.hasBall())
                .filter(e -> e.isWhite() == super.isPlayerIsWhite())
                .filter(e -> e.getCoordinate().equals(super.getOldCoordinate()))
                .findFirst();

        final Optional<Piece> p2 = board.getPieces()
                .stream()
                .filter(e -> e.isWhite() == super.isPlayerIsWhite())
                .filter(e -> e.getCoordinate().equals(super.getNewCoordinate()))
                .findFirst();


        // check if there is a piece-free line between the command's coordinates
        final boolean freeLine = board.getPieces()
                .stream()
                .noneMatch(e -> e.getCoordinate().isBetween(super.getOldCoordinate(), super.getNewCoordinate()));

        /* the ball can be moved only if :
        - there is a piece at oldC which belongs to the command's player and has the ball
        - there is a piece at newC which belongs to the command's player
        - there is a piece-free line between the command's coordinates
         */
        return p1.isPresent() && p2.isPresent() && !p1.get().getCoordinate().equals(p2.get().getCoordinate()) && freeLine &&
                (p1.get().onSameColumn(p2.get()) || p1.get().onSameRow(p2.get()) || p1.get().onSameDiagonal(p2.get()));
    }

    /**
     * undoes the action
     * @param board current board of the game
     * @throws IllegalArgumentException if the parameter is null
     * @since 1.0.0
     */
    @Override
    public void undo(final Board board) {
        if (board == null) {
            throw new IllegalArgumentException();
        }

        // the mirrored command is done
        final MoveBall tmp = new MoveBall(super.getNewCoordinate(), super.getOldCoordinate(), super.isPlayerIsWhite());
        tmp.doS(board);
    }

    /**
     * redoes the action
     * @param board current board of the game
     * @throws IllegalArgumentException if the parameter is null
     * @since 1.0.0
     */
    @Override
    public void redo(final Board board) {
        if (board == null) {
            throw new IllegalArgumentException();
        }
        
        this.doS(board);
    }
}
