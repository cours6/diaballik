package diaballik.model;

/**
 * this class represents an Ai which changes its level of difficulty during the Game
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public class ProgressiveAI extends AI {

    private int nbActionsPlayed;

    private AI ai;

    /**
     * makes the ai play a move
     * here the action is chosen by its own strategy
     * either NoobAI or StartingAI
     * @param board current board of the game
     * @param nbActions number of actions left to be played during the current turn
     * @param isPlayerWhite player's color for whom the AI plays
     * @return a command which describes the ai's move
     * @throws IllegalArgumentException if the board is null
     * @since 1.0.0
     */
    @Override
    public Command play(final Board board, final int nbActions, final boolean isPlayerWhite) {
        if(board == null) {
            throw new IllegalArgumentException();
        }
        if(this.nbActionsPlayed / 3 == 4) {
            this.ai = new StartingAI();
        }
        this.nbActionsPlayed++;
        return this.ai.play(board, nbActions, isPlayerWhite);
    }

    /**
     * constructor to create the AI with Noob level for the beginning
     */
    public ProgressiveAI() {
        this.nbActionsPlayed = 0;
        this.ai = new NoobAI();
    }
}
