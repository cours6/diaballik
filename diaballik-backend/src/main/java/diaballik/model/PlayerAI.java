package diaballik.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * this class represents an artificial player
 * @author Quentin Mazouni
 * @version 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerAI extends Player {

    @XmlTransient
    private AI strategy;

    /**
     * constructor
     * @param isWhite true if the player's color is white
     * @param name player's name
     * @param level integer between 1 and 3 which defines the difficulty of the AI
     * @throws IllegalArgumentException if name is null or empty
     * if level does not belong to [1, 3]
     * @since 1.0.0
     */
    public PlayerAI(final boolean isWhite, final String name, final int level) {
        super(isWhite, name);

        // ...
        if(level == 1) {
            this.strategy = new NoobAI();
        } else if(level == 2) {
            this. strategy = new StartingAI();
        } else if(level == 3) {
            this.strategy = new ProgressiveAI();
        } else {
            throw new IllegalArgumentException();
        }
    }

    private PlayerAI() {
        super(false, "no");
    }

    /**
     * @param board current board of the game
     * @param nbActions number of actions left for the current turn
     * @return a command which describes the ai player's move
     * @throws IllegalArgumentException if the board is null
     * if nbActions does not belong to [1, 3]
     * @since 1.0.0
     */
    public Command play(final Board board, final int nbActions) {
        if(board == null || nbActions < 1 || nbActions > 3) {
            throw new IllegalArgumentException();
        }

        return this.strategy.play(board, nbActions, this.isWhite());
    }

    /**
     * describes the nature of the player (here, always an artificial player)
     * @return false
     * @since 1.0.0
     */
    @Override
    public boolean isHumanPlayer() {
        return false;
    }

    /**
     * @param o object instance to be compared with
     * @return true if both players have the same color
     * @since 1.0.0
     */
    @Override
    public boolean equals(final Object o) {
        return super.equals(o);
    }

    /**
     * @return  hasCode
     * @since 1.0.0
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
