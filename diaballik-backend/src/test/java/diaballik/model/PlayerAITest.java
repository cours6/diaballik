package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PlayerAITest {

    private PlayerAI p1;
    private PlayerAI p1Bis;
    private PlayerAI p2;

    @BeforeEach
    void setUp() {
        p1 = new PlayerAI(true, "player1", 1);
        p1Bis = new PlayerAI(true, "player1", 2);
        p2 = new PlayerAI(false, "player2", 2);
    }

    @Test
    void playerNullNameException() {
        assertThrows(IllegalArgumentException.class, () -> new PlayerAI(true, null, 1));
    }

    @Test
    void playerEmptyNameException() {
        assertThrows(IllegalArgumentException.class, () -> new PlayerAI(true, "", 1));
    }

    @Test
    void playerWrongLevelException() {
        assertThrows(IllegalArgumentException.class, () -> new PlayerAI(true, "player", -1));
        assertThrows(IllegalArgumentException.class, () -> new PlayerAI(true, "player", 0));
        assertThrows(IllegalArgumentException.class, () -> new PlayerAI(true, "player", 4));
    }

    @Test
    void playNullException() {
        assertThrows(IllegalArgumentException.class, () -> this.p1.play(null, 1));
    }

    @Test
    void playWrongNbActionsException() {
        assertThrows(IllegalArgumentException.class, () -> this.p1.play(null, -1));
        assertThrows(IllegalArgumentException.class, () -> this.p1.play(null, 0));
        assertThrows(IllegalArgumentException.class, () -> this.p1.play(null, 4));
    }

    @Test
    void play() {
        Board board = new Board(this.p1, this.p2);
        Command command = this.p1.play(board, 3);

        assertTrue(command.canDo(board));
    }

    @Test
    void isHumanPlayer() {
        assertFalse(this.p1.isHumanPlayer());
    }

    @Test
    void equalsFalse() {
        assertNotEquals(p1, p2);
    }

    @Test
    void equalsTrue() {
        assertEquals(p1, p1Bis);
    }
}