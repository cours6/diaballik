package diaballik.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PieceTest {

    Piece pBlack;
    Piece pWhite;
    Player playerBlack;
    Player playerWhite;

    @BeforeEach
    void setUp() {
        playerBlack = new Player(false, "toto");
        playerWhite = new Player(true, "toto");

        pBlack = new Piece(playerBlack);
        pWhite = new Piece(playerWhite);

    }

    @Test
    void isWhiteFalse() {
        assertFalse(pBlack.isWhite());
    }

    @Test
    void isWhiteTrue() {
        assertTrue(pWhite.isWhite());
    }

    @Test
    void setCoordinate() {
        Coordinate c = new Coordinate(0, 3);
        pBlack.setCoordinate(c);
        assertEquals(c, pBlack.getCoordinate());
    }

    @Test
    void getCoordinateEquals() {
        Coordinate cBlack=pBlack.getCoordinate();
        Coordinate c = new Coordinate(0, 0);
        assertEquals(c, cBlack);
    }

    @Test
    void getCoordinateNotEquals() {
        Coordinate cBlack=pBlack.getCoordinate();
        Coordinate c = new Coordinate(1, 1);
        assertNotEquals(c, cBlack);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameRowTrue(int x) {
        Coordinate c1 = new Coordinate(2, 2);
        Coordinate c2 = new Coordinate(x, 2);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameRow(p2));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameRowFalse(int x) {
        Coordinate c1 = new Coordinate(2, 2);
        Coordinate c2 = new Coordinate(x, 3);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertFalse(p1.onSameRow(p2));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameColumnTrue(int y) {
        Coordinate c1 = new Coordinate(2, 2);
        Coordinate c2 = new Coordinate(2, y);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameColumn(p2));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4})
    void onSameColumnFalse(int y) {
        Coordinate c1 = new Coordinate(2, 2);
        Coordinate c2 = new Coordinate(3, y);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertFalse(p1.onSameColumn(p2));
    }

    @Test
    void onSameDiagonal1() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(3,3);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameDiagonal(p2));
    }

    @Test
    void onSameDiagonal2() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(3,1);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameDiagonal(p2));
    }

    @Test
    void onSameDiagonal3() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(1,3);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameDiagonal(p2));
    }

    @Test
    void onSameDiagonal4() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(0,0);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertTrue(p1.onSameDiagonal(p2));
    }

    @Test
    void onSameDiagonal5() {
        Coordinate c1=new Coordinate(2,2);
        Coordinate c2=new Coordinate(1,0);
        Piece p1 = new Piece(playerBlack);
        Piece p2 = new Piece(playerBlack);
        p1.setCoordinate(c1);
        p2.setCoordinate(c2);
        assertFalse(p1.onSameDiagonal(p2));
    }

    @Test
    void getPlayer1() {
        Player pB = new Player(false, "toto");
        Player p=pBlack.getPlayer();
        assertEquals(pB,p);
    }

    @Test
    void getPlayer2() {
        Player pW = new Player(true, "toto");
        Player p=pBlack.getPlayer();
        assertNotEquals(pW,p);
    }

    @Test
    void getPlayer3() {
        Player pW = new Player(true, "toto");
        Player p = pWhite.getPlayer();
        assertEquals(pW,p);
    }

    @Test
    void setBall() {
        pBlack.setBall(true);
        assertTrue(pBlack.hasBall());
    }

    @Test
    void hasBall() {
        assertFalse(pBlack.hasBall());
    }
}