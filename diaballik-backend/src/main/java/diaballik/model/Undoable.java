package diaballik.model;

/**
 * this interface describes a undoable action
 *
 * @author Guénaël Bournac
 * @version 1.0.0
 */
public interface Undoable {

    void undo(Board board);
    void redo(Board board);
}
